﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using TCP.Core;
using UDP.Core;

namespace UDP.Server
{
    public class UdpListener : UdpBase
    {
        private static readonly object padlock = new object();

        /// <summary>
        /// 9.	Zaraz po uruchomieniu ma rozpocząć nasłuchiwanie komunikatów DISCOVER na porcie 7 z 
        /// użyciem protokołu UDP i odpowiadać komunikatem OFFER ADDRESS PORT,
        /// </summary>

        private IPEndPoint _listenOn;
        public List<Tuple<string, IPAddress, int>> networkInterfacesRun { get; } = new List<Tuple<string, IPAddress, int>>();
        public UdpListener(int listeningPort) : this(new IPEndPoint(IPAddress.Any, listeningPort))
        {
            ListeningPort = listeningPort;
        }
        public int ListeningPort { get; }
        public UdpListener(IPEndPoint endpoint)
        {
            _listenOn = endpoint;
            Client = new UdpClient(_listenOn);
        }
        public string ReceiveMessage { get; set; } = "DISCOVER";
        public string SendingMessage { get; set; } = "OFFER SERVER PORT";
        public void Reply(string message, IPEndPoint endpoint)
        {
            //TODO: Konrad - sprawdz czy serwer jest zajety
            var datagram = Encoding.ASCII.GetBytes(message);
            Client.Send(datagram, datagram.Length, endpoint);
        }
        /// <summary>
        /// Przykrywamy to, ponieważ do udp serwera dostajemy się poprzez obiekt, bo tutaj nie mamy singletona
        /// </summary>
        public bool InterruptSignal { get; set; } = false;

        public async Task<bool> Process()
        {
            OnRequest(this, "UDP run", new Konrad.Core.TextFormatting());
            while (!InterruptSignal)
            {

                try
                {
                    Received received = await Receive();

                    if (Equals(received.Message, ReceiveMessage))
                    {
                        OnRequest(this, $"Odebrano sygnał DISCOVERY, wysyłam ofertę do{received.Sender}\n", new Konrad.Core.TextFormatting());
                        //TODO: Konrad - pytankoS
                        //a.	dla każdego dostępnego interfejsu sieciowego (o którym mowa w pkt. 8), wątek UDP wysyła oddzielne pakiety OFFER,
                        //To znaczy, że dla każdego interfejsu ma wysłać ofertę?
                        // Wyślę jako odpowiedź tyle pakietów ofert ile w danech chwili jest uruchomionych na interfejsach sieciowych?
                        foreach (var networkIntterfaces in networkInterfacesRun)
                            Reply(networkIntterfaces.ToString(), received.Sender);
                    }
                }
                catch (Exception ex)
                {

                }
            }
            OnRequest(Helper.InterruptServerSignal, null, null);
            return true;
        }
    }
}
