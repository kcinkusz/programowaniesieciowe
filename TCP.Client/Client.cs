﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using TCP.Core;

namespace TCP.Client
{
    public class Client : Application, IClient
    {
        private static readonly object padlock = new object();
        private static Client instance = null;
        private ClientState clientState = ClientState.ClientNotRunned;
        private string clientTestMessage = DateTime.Now.Second.ToString();
        private Client()
        {
        }

        public static Client Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new Client();
                    }
                    return instance;
                }
            }
        }

        public ClientState ClientState
        {
            get { return clientState; }
            set
            {
                clientState = value;
                OnPropertyChanged(clientState.ToString());
            }
        }

        Guid IClient.Guid => base.Guid;
        private const string sending = "Wysyłam...";
        private const string sendedSuccesful = "Wysłałem!";
        private const string longMessage =
            @"Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, 
            totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. 
            Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, 
            qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit, amet, consectetur, 
            adipisci velit, sed quia non numquam eius modi tempora incidunt, ut labore et dolore magnam aliquam quaerat voluptatem. 
            Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea 
            commodi consequatur? Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae 
            consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur?
            [33] At vero eos et accusamus et iusto odio dignissimos ducimus, qui blanditiis praesentium voluptatum deleniti atque 
            corrupti, quos dolores et quas molestias excepturi sint, obcaecati cupiditate non provident, similique sunt in culpa, 
            qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita 
            distinctio. Nam libero tempore, cum soluta nobis est eligendi optio, cumque nihil impedit, quo minus id, quod maxime placeat, 
            facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut 
            rerum necessitatibus saepe eveniet, ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur 
            a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.";
        /// <summary>
        /// Czy wysyłać randomowe liczby, jesli tak to sprawdz z jakim interwałem
        /// </summary>
        public Tuple<bool, int> SendingNumber { get; set; } = new Tuple<bool, int>(false, -1);

        private const int tryAttempt = 100; // próbuj wysłać komunikat 100 razy
        public async Task<bool> Process()
        {
            OnRequest(this, "Uruchomiono proces klienta", new Konrad.Core.TextFormatting { ColorName = "Green" });

            ClientState = ClientState.ClientNotRunned;
            DateTime pauseTime = DateTime.MinValue;
            TcpClient server = new TcpClient();
            try
            {
                //tcpClient =
                //tcpClient.Connect(remoteIPAddress, port);
                IPEndPoint ipEndPoint = new IPEndPoint(address, portNumber);
                //server = new TcpClient(ipEndPoint);
                server.Connect(ipEndPoint);
                while (!InterruptSignal)
                {
                    if (Pause)
                    {
                        if (ClientState != ClientState.ClientPause)
                        {
                            ClientState = ClientState.ClientPause;
                            pauseTime = DateTime.Now;
                            OnRequest(this, $"Zatrzymano wykonanie logiki zadania o {pauseTime}, następuje proces bezczynności", new Konrad.Core.TextFormatting());
                            OnRequest(Helper.PauseClientSignal, null, null);// string.Empty, new Konrad.Core.TextFormatting());
                        }
                        continue;
                    }
                    else if (pauseTime != DateTime.MinValue)
                    {
                        OnRequest(this, $"Wątek wznowiono po {(DateTime.Now - pauseTime).Milliseconds} milisekundach", new Konrad.Core.TextFormatting());
                        // pauseTime = DateTime.MinValue;
                    }

                    ClientState = ClientState.ClientReady;
                    try
                    {
                        var adress = Helper.GetIPAddressesOfRouteComputerInterfaces().Last().ToString();//czemu ostani? - bo domyślnie w serwerze też jest coś takiego

                        OnRequest(this, "Trwa próba nawiązania połączenia, wyślij jakieś liczby", new Konrad.Core.TextFormatting { ColorName = "OrangeRed" });

                        //TcpClient server = new TcpClient(adress, Helper.DEFAULT_PORT_NO);

                        ClientState = ClientState.ClientBusy;
                        if (server.Available > 0)
                        {
                           NetworkStream nwStream = server.GetStream();
                            byte[] bytesToRead = new byte[server.ReceiveBufferSize];
                            int bytesRead = nwStream.Read(bytesToRead, 0, server.ReceiveBufferSize);
                            OnRequest(this, Encoding.ASCII.GetString(bytesToRead, 0, bytesRead), new Konrad.Core.TextFormatting { ColorName = "OrangeRed" });
                            throw new Exception("Serwer przetwarza klienta, proces zostaje zakończony");
                        }
                        else
                        if (server.Connected)
                        {
                            NetworkStream nwStream = server.GetStream();
                            if (SendingNumber.Item1)
                            {
                                Random random = new Random();
                                byte[] bytesToSend = ASCIIEncoding.ASCII.GetBytes(random.NextDouble().ToString());// $"{Name}:{mes}");
                                OnRequest(this, sending, new Konrad.Core.TextFormatting());
                                nwStream.Write(bytesToSend, 0, bytesToSend.Length);
                                OnRequest(this, sendedSuccesful, new Konrad.Core.TextFormatting());
                                ClientState = ClientState.ClientBusy;
                                Task.Delay(SendingNumber.Item2 * 1000);
                            }

                            //int i = 0;
                            //while (i++ < tryAttempt)
                            //{
                            //    byte[] bytesToSend = ASCIIEncoding.ASCII.GetBytes(longMessage);// $"{Name}:{mes}");
                            //    OnRequest(this, sending, new Konrad.Core.TextFormatting());
                            //    nwStream.Write(bytesToSend, 0, bytesToSend.Length);
                            //    OnRequest(this, sendedSuccesful, new Konrad.Core.TextFormatting());
                            //    ClientState = ClientState.ClientBusy;
                            //}
                        }
                        else
                        {
                        }
                    }
                    catch (Exception ex) //TODO: Konrad - tutaj leci wyjątek podczas próby nawiązania połączenia przez udp
                    {
                        OnRequest(this, $"Nie udało się podłączyć do żadnego serwera błąd: {ex}", new Konrad.Core.TextFormatting { ColorName = "PaleVioletRed" });
                    }
                    finally
                    {
                        if (pauseTime != DateTime.MinValue)
                        {

                            OnRequest(Helper.PauseClientSignal, null, null);// string.Empty, new Konrad.Core.TextFormatting());
                            pauseTime = DateTime.MinValue;
                        }
                    }
                    await Task.Delay(1200);
                }
            }
            catch (Exception ex)
            {
                OnRequest(this, ex.ToString(), null);
            }

            server.Close();
            server.Dispose();
            OnRequest(Helper.InterruptClientSignal, null, null);
            OnRequest(this, "Otrzymano sygnał przerwania, proces zostaje zamknięty", new Konrad.Core.TextFormatting()); //TODO: Konrad to następuje z opóźnieniem, więc wypada zrobić jeszcze 
            ClientState = ClientState.ClientStopped;
            return true;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            Helper.GetIpOfAllNetworkInterfaces().ToList().ForEach(x => sb.Append($"\n{x.Item1}:\n-{x.Item2}\n"));
            return sb.ToString(); ;
        }
    }
}