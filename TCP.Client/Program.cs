﻿using Konrad.Core;
using System;
using System.Threading;
using System.Threading.Tasks;
using TCP.Core;

namespace TCP.Client
{
    internal class Program
    {
        private static CancellationTokenSource cancelToken = new CancellationTokenSource();

        private static void Main(string[] args)
        {
            Console.CancelKeyPress += Console_CancelKeyPress;

            Random rnd = new Random(BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0));
            int port = Math.Abs(rnd.Next((int)Math.Pow(2, 16) - 1));

            Client client = Client.Instance;
            client.Name = "First";
            Task clientTask = Task.Factory.StartNew(() => client.Process(), cancelToken.Token);
            client.Request += Client_Request;

            while (!cancelToken.IsCancellationRequested) ;
        }

        private static void Client_Request(object sender, string requestMessage, TextFormatting textFormatting)
        {
            if (requestMessage == Helper.InterruptClientSignal)
            {
                Console.Beep();
                Console.BackgroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("Odebrano sygnał przerwania pracy klienta, dziękujemy za korzystanie z aplikacji");
                cancelToken.Cancel(false);
            }
            else
                Console.WriteLine(requestMessage);
        }

        private static void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            e.Cancel = true;
            Application.InterruptSignal = true;
        }
    }
}