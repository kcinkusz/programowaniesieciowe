﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Konrad.Winforms.Extensions
{
    public static class RichTextBoxExtensions
    {
        public static void AppendText(this RichTextBox box, string text, Color color)
        {
            box.SelectionStart = box.TextLength;
            box.SelectionLength = 0;

            box.SelectionColor = color;
            box.AppendText(text);
            box.SelectionColor = box.ForeColor;
        }

        private const int richTextBoxMaxCapacity = 15_000;
        private const int richTextBoxOffsetCleaner = 10_000;

        public static Action AppendToRTB(RichTextBox richTextBox, TextFormatting textformat, string text, bool withScrolling)
        {
            return new Action(() =>
            {
                if (richTextBox.TextLength > richTextBoxMaxCapacity)
                    richTextBox.Text = new String(richTextBox.Text.Skip(richTextBoxOffsetCleaner).ToArray());
                richTextBox.AppendText(text, textformat.Color);
                if (withScrolling)
                    richTextBox.ScrollToCaret();
            });
        }
    }
}