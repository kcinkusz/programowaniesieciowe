﻿using System.Drawing;

namespace Konrad.Winforms
{
    public class TextFormatting : Konrad.Core.TextFormatting
    {
        public TextFormatting()
        {

        }
        public TextFormatting(Konrad.Core.TextFormatting text)
        {
            ColorName = text.ColorName;
        }

        private Color color = Color.Red;

        public Color Color
        {
            get
            {
                color = Color.FromName(ColorName);
                return color;
            }
            set { ColorName = value.ToString(); }
        }
    }
}