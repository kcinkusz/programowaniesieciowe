﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;

namespace TCP.Core
{
    
    public static class Helper
    {
        public const int DEFAULT_PORT_NO = 5000;
        public const string ReceivedData = "Received data:";
        public const string NewLine = "\n";
        public const string Sleep = "Czekam na połącznie przychodzące...";
        public const string EndServerWork = "End serverbasic work";
        public const string InterruptClientSignal = "B34D784F-4ED0-4CDE-971D-D81F979915E5";
        public const string PauseClientSignal = "BF32B5EF-47EB-40F8-9C06-BD249EDB664B";
        public const string InterruptServerSignal  = "45F9FE72-4380-471C-905B-E8598F12CE44";
        public const string PauseServerSignal = "2A9EBCD7-51B5-4755-A795-CD0F684F2160";

        /// <summary>
        /// Sygnał, który wysyła runner, kiedy zaczyna odbierać komunikaty od klienta
        /// </summary>
        public const string RunnerStartReceivingSignal = "B3C50A9A-1514-48C5-B62C-8F68DA1C3196";
        public static IPEndPoint globalEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), DEFAULT_PORT_NO);
        public static string simpleCommunicate = "OFFER ADDRESS PORT";
        public static byte[] simpleCommunicateBytes = Encoding.ASCII.GetBytes(simpleCommunicate);

        public static NetworkInterface[] AllInterfaces { get; } = NetworkInterface.GetAllNetworkInterfaces();

        public static IEnumerable<Tuple<string, IPAddress>> GetIpOfAllNetworkInterfaces()
        {
            foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 || ni.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                {
                    foreach (UnicastIPAddressInformation ip in ni.GetIPProperties().UnicastAddresses)
                    {
                        if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                        {
                            yield return new Tuple<string, IPAddress>(ni.Name, ip.Address);
                        }
                    }
                }
            }
        }
        
        public static IPAddress[] GetIPAddressesOfRouteComputerInterfaces()
        {
            String strHostName = Dns.GetHostName();
            IPHostEntry iphostentry = Dns.GetHostByName(strHostName);
            return iphostentry.AddressList;
        }
    }
}