﻿using System;

namespace TCP.Core
{
    public interface IClient : IWorkflow
    {
        ClientState ClientState { get; set; }

        Guid Guid { get; }
    }
}