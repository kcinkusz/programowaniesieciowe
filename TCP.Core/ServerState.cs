﻿namespace TCP.Core
{
    public enum ServerState //analogicznie w kliencie kliencstate
    {
        NotRunned,
        Ready,
        Busy,
        Pause,
        Stopped,
        Initialized,
    }
}