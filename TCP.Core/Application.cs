﻿using Konrad.Core;
using System;
using System.ComponentModel;
using System.Net;
using System.Text;

namespace TCP.Core
{
    public delegate void Request(object sender, string requestMessage, TextFormatting messageFormatting);

    public abstract class Application
    {
        protected readonly Guid Guid;

        protected Application()
        {
            Guid = Guid.NewGuid();
        }

        public event Request Request;

        public static bool InterruptSignal { get; set; } = true;
        public static bool Pause { get; set; } = false;
        public string Name { get; set; }
        public int portNumber { get; set; }
        public IPAddress address { get; set; } = null;
        public IPEndPoint endPoint { get; set; } = null;

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        protected virtual void OnRequest(object sender, string message, TextFormatting messageFormatting, bool withDate = true)
        {
            StringBuilder sb = new StringBuilder();
            if (withDate)
                sb.Append($"[{(DateTime.Now)}]: ");
            sb.Append($"{message}");
            Request?.Invoke(sender, sb.ToString(), messageFormatting);
        }
    }
}