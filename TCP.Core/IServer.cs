﻿using System;

namespace TCP.Core
{
    public interface IServer : IWorkflow
    {
        ServerState ServerState { get; set; }
        Guid Guid { get; }
    }
}