﻿using System;

namespace TCP.Core
{
    public class ColorHelper
    {
        public string[] ColorNames = new string[]
        {
            "DarkViolet",
            "MediumPurple",
            "MediumSeaGreen",
            "MediumSlateBlue",
            "MediumSpringGreen",
            "MediumTurquoise",
            "MediumVioletRed",
            "MidnightBlue",
            "MediumOrchid",
            "MintCream",
            "Moccasin",
            "NavajoWhite",
            "Navy",
            "OldLace",
            "Olive",
            "OliveDrab",
            "Orange",
            "MistyRose",
            "OrangeRed",
            "MediumBlue",
            "Maroon",
            "LightBlue",
            "LightCoral",
            "LightGoldenrodYellow",
            "LightGreen",
            "LightGray",
            "LightPink",
            "LightSalmon",
            "MediumAquamarine",
            "LightSeaGreen",
            "LightSlateGray",
            "LightSteelBlue",
            "LightYellow",
            "Lime",
            "LimeGreen",
            "Linen",
            "Magenta",
            "LightSkyBlue",
            "LemonChiffon",
            "Orchid",
            "PaleGreen",
            "SlateBlue",
            "SlateGray",
            "Snow",
            "SpringGreen",
            "SteelBlue",
            "Tan",
            "Teal",
            "SkyBlue",
            "Thistle",
            "Turquoise",
            "Violet",
            "Wheat",
            "White",
            "WhiteSmoke",
            "Yellow",
            "YellowGreen",
            "Tomato",
            "PaleGoldenrod",
            "Silver",
            "SeaShell",
            "PaleTurquoise",
            "PaleVioletRed",
            "PapayaWhip",
            "PeachPuff",
            "Peru",
            "Pink",
            "Plum",
            "Sienna",
            "PowderBlue",
            "Red",
            "RosyBrown",
            "RoyalBlue",
            "SaddleBrown",
            "Salmon",
            "SandyBrown",
            "SeaGreen",
            "Purple",
            "LawnGreen",
            "LightCyan",
            "Lavender",
            "DarkKhaki",
            "DarkGreen",
            "DarkGray",
            "DarkGoldenrod",
            "DarkCyan",
            "DarkBlue",
            "Cyan",
            "Crimson",
            "Cornsilk",
            "LavenderBlush",
            "Coral",
            "Chocolate",
            "Chartreuse",
            "DarkMagenta",
            "CadetBlue",
            "Brown",
            "BlueViolet",
            "Blue",
            "BlanchedAlmond",
            "Black",
            "Bisque",
            "Beige",
            "Azure",
            "Aquamarine",
            "Aqua",
            "AntiqueWhite",
            "AliceBlue",
            "Transparent",
            "BurlyWood",
            "DarkOliveGreen",
            "CornflowerBlue",
            "DarkOrchid",
            "Khaki",
            "Ivory",
            "DarkOrange",
            "Indigo",
            "IndianRed",
            "HotPink",
            "Honeydew",
            "GreenYellow",
            "Green",
            "Gray",
            "Goldenrod",
            "GhostWhite",
            "Gainsboro",
            "Fuchsia",
            "Gold",
            "FloralWhite",
            "DarkRed",
            "DarkSalmon",
            "DarkSeaGreen",
            "ForestGreen",
            "DarkSlateGray",
            "DarkTurquoise",
            "DarkSlateBlue",
            "DeepPink",
            "DeepSkyBlue",
            "DimGray",
            "DodgerBlue",
            "Firebrick",
        };

        private static readonly object padlock = new object();
        private static ColorHelper instance = null;
        private readonly Random rand;
        private ColorHelper()
        {
            int seed = Guid.NewGuid().GetHashCode();

            int j = BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0);

            rand = new Random(seed);
        }

        public static ColorHelper Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new ColorHelper();
                    }
                    return instance;
                }
            }
        }

        public string DrawColorName()
        {
            string colorName = "Black";
            try
            {
                int colorNamesIndex = rand.Next(0, ColorNames.Length - 1);
                colorName = ColorNames[colorNamesIndex];
            }
            catch (Exception ex)
            {
                //TODO: add logging information, nie udało się pobrać nazwy koloru
            }

            return colorName;
        }
    }
}