﻿namespace TCP.Core
{
    public enum ClientState //analogicznie w kliencie kliencstate
    {
        ClientInitialized,
        ClientNotRunned,
        ClientReady,
        ClientBusy,
        ClientPause,
        ClientStopped,
    }
}