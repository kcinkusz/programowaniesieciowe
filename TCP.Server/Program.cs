﻿using Konrad.Console;
using System;
using System.Threading;
using System.Threading.Tasks;
using TCP.Core;

namespace TCP.Server
{
    internal class Program
    {
        private static CancellationTokenSource cancelToken = new CancellationTokenSource();

        private static void Main(string[] args)
        {
            System.Console.CancelKeyPress += Console_CancelKeyPress;

            Random rnd = new Random(BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0));
            int port = Math.Abs(rnd.Next((int)Math.Pow(2, 16) - 1));

            Server server = Server.Instance;
            server.portNumber = port;
            Task serverTask = Task.Factory.StartNew(() => server.Process(), cancelToken.Token);
            server.Request += ServerBasic_Request;

            while (!cancelToken.IsCancellationRequested) ;
        }

        private static void ServerBasic_Request(object sender,string requestMessage, Konrad.Core.TextFormatting textFormatting)
        {
            TextFormatting text = (TextFormatting)textFormatting;

            if (requestMessage == Helper.InterruptServerSignal)
            {
                Console.Beep();
                Console.BackgroundColor = ConsoleColor.DarkRed;
                System.Console.WriteLine("Odebrano sygnał przerwania pracy serwera, dziękujemy za korzystanie z aplikacji");
                cancelToken.Cancel(false);
            }
            else
                System.Console.WriteLine(requestMessage);
        }

        private static void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            e.Cancel = true;
            Server.InterruptSignal = true;
        }
    }
}