﻿using System;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TCP.Core;

namespace TCP.Server
{
    public class Server : Application, IServer
    {
        private static readonly object padlock = new object();
        private static Server instance = null;
        private ServerState serverState = ServerState.NotRunned;

        /// <summary>
        /// Nasłuchuj na wszystkich interfejsach sieciowych
        /// </summary>
        public bool ListenOnAllPorts { get; set; } //TODO: przenieść gdzie indziej, bo tutaj nie powinno być stricte aplikacyjnych flag
        /// <summary>
        /// DISCOVER na porcie 7
        /// </summary>
        public bool DISCOVER { get; set; }
        /// <summary>
        /// Co jaki czas mają być odbierane dane
        /// </summary>
        public TimeSpan RequestIntervalDelay { get; private set; } = new TimeSpan(100000);

        /// <summary>
        /// Jak długo serwer ma czekać na następną próbę połączenia
        /// </summary>
        public int ServerSleepDelay { get; set; } = 500;

        private Server()
        {
            serverRunners = new ConcurrentQueue<ServerRunner>();
        }

        public static Server Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new Server();
                    }
                    return instance;
                }
            }
        }

        Guid IServer.Guid => base.Guid;

        public ServerState ServerState
        {
            get { return serverState; }
            set
            {
                serverState = value;
                OnPropertyChanged(serverState.ToString());
            }
        }

        public ObservableCollection<Task> ServerTaskCollection { get; set; } = new ObservableCollection<Task>();
        private readonly ConcurrentQueue<ServerRunner> serverRunners;
        public bool IsServerRunnersActive => serverRunners.Any();
        private Task serverRunnersObserver;
        private CancellationToken cancelServerRunners;
        public ConcurrentQueue<object> concurrentQueue = new ConcurrentQueue<object>();
        /// <summary>
        /// 1) jeden interfejs na porcie 5000
        /// 2) jeden interfejs na losowym porcie 
        /// 3) wszystkie interfejsy na losowych portach
        /// 
        /// równolegle
        /// 1) DISCOVER na porcie 7
        /// </summary>
        /// <returns></returns>
        public async Task<bool> Process()
        {
            serverRunnersObserver = Task.Factory.StartNew(async () =>
            {
                while (true)
                {
                    ServerRunner currentRunner;
                    while (!serverRunners.TryDequeue(out currentRunner)) ;
                    currentRunner.Run();
                    //await currentRunner.Run();
                    while (currentRunner.State != ServerRunnerState.Closed)
                    {
                        await Task.Delay(RequestIntervalDelay);
                    }
                    object outerInfo;
                    concurrentQueue.TryDequeue(out outerInfo);
                }
            }, cancelServerRunners).ContinueWith((runnerObserver) =>
            {
                ServerRunner ignored;
                while (serverRunners.TryDequeue(out ignored)) ;
            });

            if (this.address == null)
                this.address = Helper.GetIPAddressesOfRouteComputerInterfaces().Last();

            if (this.endPoint == null)
                this.endPoint = new IPEndPoint(address, Helper.DEFAULT_PORT_NO);

            TcpListener listener = new TcpListener(endPoint);
            bool listenerActive = true;
            try
            {
                listener.ExclusiveAddressUse = true;
                //listener.Server.Disconnect(true);
                //listener.Server.Close();
                listener.Start();
                OnRequest(this, $"Uruchomiono server na interfejsie o IP: {address}, na porcie {endPoint.Port}", new Konrad.Core.TextFormatting());
            }
            catch (SocketException se)
            {
                OnRequest(this, 
                    "Wybierz inny interfejs sieciowy - tego program " +
                    "nie obsługuje.\n Zatrzymano prace serwera, ponieważ na wybranym IP wystąpił wyjątek:\n" 
                    + se.ToString(), new Konrad.Core.TextFormatting());
                listenerActive = false;
            }
            catch (Exception ex)
            {
                OnRequest(this, ex.ToString(), null);
            }

            int counter = 0, sleepingcounter = 0;

            DateTime pauseTime = DateTime.Now, sleepTime = DateTime.Now;

            ServerState = ServerState.Ready;

            while (!InterruptSignal && listenerActive)
            {
                if (!Pause & counter > 0)
                {
                    OnRequest(this, $"Wznowiono pracę, czekano {counter} pętli oraz {(DateTime.Now - pauseTime).Milliseconds} milisekund", new Konrad.Core.TextFormatting());
                    OnRequest(Helper.PauseServerSignal, null, null);
                    counter = 0;
                    ServerState = ServerState.Ready;
                }
                if (listener.Pending() && !Pause)
                {
                    ServerState = ServerState.Busy;
                    OnRequest(this, $"Nawiązano połączenie po {sleepingcounter} pętli, oraz {(DateTime.Now - sleepTime).Milliseconds} milisekund nastąpi próba odczytu:", new Konrad.Core.TextFormatting());
                    sleepingcounter = 0;
                    try
                    {
                        if (!concurrentQueue.IsEmpty)
                        {
                            TcpClient client = listener.AcceptTcpClient();
                            NetworkStream nwStream = client.GetStream();
                            Byte[] sendBytes = Encoding.ASCII.GetBytes("Serwer przetwarza klienta");
                            nwStream.Write(sendBytes, 0, sendBytes.Length);
                            client.Close();
                        }
                        else
                        {
                            ServerRunner serverRunner = new ServerRunner(listener);
                            serverRunner.Request += ServerRunner_Request;

                            concurrentQueue.Enqueue(serverRunner.ID);
                            serverRunners.Enqueue(serverRunner);
                        }
                    }
                    catch (Exception ex)
                    {
                    }

                    OnRequest(this, "Receive completed successfully", new Konrad.Core.TextFormatting());
                    ServerState = ServerState.Ready;
                }
                else if (Pause)
                {
                    if (ServerState != ServerState.Pause)
                    {
                        ServerState = ServerState.Pause;
                        pauseTime = DateTime.Now;

                        OnRequest(Helper.PauseServerSignal, null, null);
                    }
                    if (counter <= 0)
                        OnRequest(this, "Wątek zatrzymany, czekam na wznowienie", new Konrad.Core.TextFormatting());
                    counter++;

                }
                else
                {
                    if (sleepingcounter <= 0)
                    {
                        OnRequest(this, Helper.Sleep, new Konrad.Core.TextFormatting());
                        sleepTime = DateTime.Now;
                    }
                    sleepingcounter++;
                }
                await Task.Delay(RequestIntervalDelay);
            }


            listener.Stop();
            CancellationTokenSource source = new CancellationTokenSource();
            cancelServerRunners = source.Token;
            source.Cancel();

            ServerState = ServerState.Stopped;
            OnRequest(this, "Otrzymałem sygnał przerwania, zatrzymuje serwer", new Konrad.Core.TextFormatting());
            ServerRunner runner;
            while (serverRunners.TryDequeue(out runner))
            {
                Console.WriteLine(runner);
            }
            OnRequest(Helper.InterruptServerSignal, null, null);
            return true;
        }

        private void ServerRunner_Request(object sender, string requestMessage, Konrad.Core.TextFormatting text)
        {
            OnRequest(sender, requestMessage, text);
            //if (sender is ServerRunner)
            //    OnRequest(sender as ServerRunner, requestMessage, text);
            //else if (sender is ReceivedClientInformation)
            //    OnRequest(sender as ReceivedClientInformation, requestMessage, text);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            Helper.GetIpOfAllNetworkInterfaces().ToList().ForEach(x => sb.Append($"\n{x.Item1}:\n-{x.Item2}\n"));
            return sb.ToString(); ;
        }
    }
}