﻿using Konrad.Core;
using System;
using System.IO;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TCP.Core;

namespace TCP.Server
{
    public enum ServerRunnerState
    {
        None,
        Connecting,
        Connected,
        Receiving,
        Closing,
        Closed,
    }

    public class ReceivedClientInformation
    {
        private readonly StringBuilder clientInformationMessage;

        public ReceivedClientInformation(TcpClient client, NetworkStream stream)
        {
            clientInformationMessage = new StringBuilder();
            try
            {
                clientInformationMessage.AppendLine($"Remote end point: {client.Client.RemoteEndPoint.ToString()}");
                clientInformationMessage.AppendLine($"Local end point: {client.Client.LocalEndPoint.ToString()}");
                var pi = stream.GetType().GetProperty("Socket", BindingFlags.NonPublic | BindingFlags.Instance);
                clientInformationMessage.AppendLine($"Socket information: {((Socket)pi.GetValue(stream, null)).RemoteEndPoint.ToString()}");
            }
            catch (Exception)
            {
                clientInformationMessage.AppendLine("Brak danych");
            }
        }

        public override string ToString()
        {
            return $"Informacje o kliencie:{clientInformationMessage}";
        }
    }

    public delegate void ServerRunnerStateRequest(ServerRunnerState serverState);

    public delegate void ServerRunnerRequest(object sender, string requestMessage, TextFormatting text);

    /// <summary>
    /// Funktor serwerowy
    /// Służy do bezpośredniego odbierania komunikatów od klientów
    /// </summary>
    public sealed class ServerRunner
    {
        private TcpClient client;
        private readonly TcpListener tcpListener;
        public readonly Guid ID;
        public ServerRunner(TcpListener listener)
        {
            tcpListener = listener;
            ID = Guid.NewGuid();
        }

        public TimeSpan RequestIntervalDelay { get; private set; } = new TimeSpan(100000);
        public ServerRunnerState State { get; private set; } = ServerRunnerState.None;

        public async void Run()
        {
            
            client = tcpListener.AcceptTcpClient();
            //TODO: Znowu tutaj jest dworako rozwiązane, powiadomienie o starcie odbierania jest wysłane za pomocą sygnału oraz zdarzenia
            OnStateRequest(ServerRunnerState.Connecting);
            OnRequest(this, Helper.RunnerStartReceivingSignal, new Konrad.Core.TextFormatting() { ColorName = "SpringGreen" });

            var totalBytes = client.ReceiveBufferSize;
            NetworkStream stream = client.GetStream();
            const int arrSize = 256;
            int offset = 0;
            byte[] temporaryByteArr = new byte[arrSize];

            StringBuilder sb = new StringBuilder();
            int read = 0;

            ReceivedClientInformation information = new ReceivedClientInformation(client, stream);
            OnRequest(information, information.ToString(), new TextFormatting() { ColorName = "Blue" });

            using (MemoryStream ms = new MemoryStream())
            {
                OnStateRequest(ServerRunnerState.Connected);
                while ((read = await stream.ReadAsync(temporaryByteArr, 0, arrSize)) > 0)
                {
                    OnStateRequest(ServerRunnerState.Receiving);
                    ms.Write(temporaryByteArr, 0, read);
                    sb.Clear();
                    for (int i = 0; i < arrSize; i++)
                        if (Convert.ToChar(temporaryByteArr[i]).ToString() != "\0")
                            sb.Append(Convert.ToChar(temporaryByteArr[i]));
                        else
                        {
                            break;
                        }

                    OnRequest(this, $"Odbieram dane...\n{sb}", new Konrad.Core.TextFormatting() { ColorName = "SpringGreen" });

                    offset += arrSize;
                    await Task.Delay(RequestIntervalDelay);
                }

                OnStateRequest(ServerRunnerState.Closing);
            }

            OnStateRequest(ServerRunnerState.Closed);
            client.Close();
        }

        public event ServerRunnerStateRequest StateRequest;

        public event ServerRunnerRequest Request;

        private void OnRequest(object sender, string message, TextFormatting messageFormatting) => Request?.Invoke(sender, message, messageFormatting);

        private void OnStateRequest(ServerRunnerState state) => StateRequest?.Invoke(state);
    }
}