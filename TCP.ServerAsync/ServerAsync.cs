﻿using Konrad.Core;
using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using TCP.Core;

namespace TCP.ServerAsync
{
    public class ServerAsync : Application, IServer
    {
        private static readonly object padlock = new object();
        private static ServerAsync instance = null;
        private ManualResetEvent allDone = new ManualResetEvent(false);

        private ServerAsync()
        {
        }

        public static ServerAsync Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new ServerAsync();
                    }
                    return instance;
                }
            }
        }

        Guid IServer.Guid => base.Guid;
        public ServerState ServerState { get; set; } = ServerState.NotRunned;

        public void AcceptCallback(IAsyncResult ar)
        {
            // Signal the main thread to continue.
            allDone.Set();
            Thread.Sleep(1000);
        }

        public Task<bool> Process()
        {
            IPAddress address = Helper.GetIPAddressesOfRouteComputerInterfaces().Last();
            IPEndPoint localEndPoint = new IPEndPoint(address, Helper.DEFAULT_PORT_NO);

            Socket listener = new Socket(address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(100);
                while (true)
                {
                    // Set the event to nonsignaled state.
                    allDone.Reset();
                    // Start an asynchronous socket to listen for connections.
                    OnRequest(this, "Waiting for a connection...", new TextFormatting());
                    listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);
                    // Wait until a connection is made before continuing.
                    allDone.WaitOne();
                }
            }
            catch (Exception ex)
            {
                OnRequest(this, ex.ToString(), new TextFormatting());
            }

            OnRequest(this, "Zakończono prace wątku serwera", new TextFormatting());
            return null;
        }
    }
}