﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UDP
{
    /// <summary>
    /// https://stackoverflow.com/questions/19786668/c-sharp-udp-socket-client-and-server#19787486
    /// </summary>
    class Program
    {
        public struct Received
        {
            public IPEndPoint Sender;
            public string Message;
        }

        abstract class UdpBase
        {
            protected UdpClient Client;

            protected UdpBase()
            {
                Client = new UdpClient();
            }

            public async Task<Received> Receive()
            {
                var result = await Client.ReceiveAsync();
                return new Received()
                {
                    Message = Encoding.ASCII.GetString(result.Buffer, 0, result.Buffer.Length),
                    Sender = result.RemoteEndPoint
                };
            }
        }

        //Server
        class UdpListener : UdpBase
        {
            private IPEndPoint _listenOn;

            public UdpListener(int listeningPort) : this(new IPEndPoint(IPAddress.Any, listeningPort))
            {
            }

            public UdpListener(IPEndPoint endpoint)
            {
                _listenOn = endpoint;
                Client = new UdpClient(_listenOn);
            }

            public void Reply(string message, IPEndPoint endpoint)
            {
                var datagram = Encoding.ASCII.GetBytes(message);
                Client.Send(datagram, datagram.Length, endpoint);
            }

        }

        //Client
        class UdpUser : UdpBase
        {
            private UdpUser() { }

            public static UdpUser ConnectTo(string hostname, int port)
            {
                var connection = new UdpUser();
                connection.Client.Connect(hostname, port);
                return connection;
            }

            public void Send(string message)
            {
                var datagram = Encoding.ASCII.GetBytes(message);
                Client.Send(datagram, datagram.Length);
            }

        }
        public static void Do1()
        {
            int portNumber = 7;
            //create a new server
            var server = new UdpListener(portNumber);

            //start listening for messages and copy the messages back to the client
            string messageReceived = "DISCOVER";
            string replyMessage = "OFFER ADDRESS PORT";

            Task.Factory.StartNew(async () =>
            {
                while (true)
                {
                    var received = await server.Receive();
                    if (string.Equals(received, messageReceived))
                    {
                        server.Reply(replyMessage, received.Sender);
                    }

                    if (received.Message == "quit")
                        break;
                }
            });

            //create a new client
            var client = UdpUser.ConnectTo("127.0.0.1", portNumber);

            //wait for reply messages from server and send them to console 
            Task.Factory.StartNew(async () =>
            {
                while (true)
                {
                    try
                    {
                        var received = await client.Receive();
                        Console.WriteLine(received.Message);
                        if (received.Message.Contains("quit"))
                            break;
                    }
                    catch (Exception ex)
                    {
                        //   Debug.Write(ex);
                    }
                }
            });

            //type ahead :-)
            string read;
            do
            {
                read = Console.ReadLine();
                client.Send(read);
            } while (read != "quit");
        }
        static void Main(string[] args)
        {

            int portNumber = 7;
            //create a new client
            var client = UdpUser.ConnectTo("127.0.0.1", portNumber);

            //wait for reply messages from server and send them to console 
            Task.Factory.StartNew(async () =>
            {
                while (true)
                {
                    try
                    {
                        var received = await client.Receive();
                        Console.WriteLine(received.Message);
                        if (received.Message.Contains("quit"))
                            break;
                    }
                    catch (Exception ex)
                    {
                        //   Debug.Write(ex);
                    }
                }
            });

            //type ahead :-)
            string read;
            do
            {
                read = Console.ReadLine();
                client.Send(read);
            } while (read != "quit");
        }
        /// <summary>
        /// https://stackoverflow.com/questions/9416241/how-to-send-a-message-and-receive-a-response-on-the-same-socketS
        /// </summary>
        public static void Do2()
        {
            var broadcastMessage = Encoding.UTF8.GetBytes("Hello multicast!");
            var multicastAddress = IPAddress.Parse("239.255.255.250");
            var signal = new ManualResetEvent(false);
            var multicastPort = 1900;

            using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp))
            {
                var multicastEp = new IPEndPoint(multicastAddress, multicastPort);
                EndPoint localEp = new IPEndPoint(IPAddress.Any, multicastPort);

                // Might want to set this:
                //socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, 1); 
                socket.Bind(localEp);
                socket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.AddMembership, new MulticastOption(multicastAddress, IPAddress.Any));
                // May want to set this:
                //socket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.MulticastTimeToLive, 0); // only LAN
                var thd = new Thread(() =>
                {
                    var response = new byte[8000];
                    socket.ReceiveFrom(response, ref localEp);
                    var str = Encoding.UTF8.GetString(response).TrimEnd('\0');
                    Console.WriteLine("[RECV] {0}", str);
                    signal.Set();
                    Console.WriteLine("Receiver terminating...");
                });
                signal.Reset();
                thd.Start();

                socket.SendTo(broadcastMessage, 0, broadcastMessage.Length, SocketFlags.None, multicastEp);
                Console.WriteLine("[SEND] {0}", Encoding.UTF8.GetString(broadcastMessage));
                signal.WaitOne();
                Console.WriteLine("Multicaster terminating...");
                socket.Close();
                Console.WriteLine("Press any key.");
                Console.ReadKey();
            }
        }
    }
}
