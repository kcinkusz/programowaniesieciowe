﻿using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using TCP.Core;
using UDP.Core;

namespace UDP
{
    public abstract class UdpBase :  Application
    {
        protected UdpClient Client;

        protected UdpBase()
        {
            Client = new UdpClient();
        }

        public async Task<Received> Receive()
        {
            var result = await Client.ReceiveAsync();
            return new Received()
            {
                Message = Encoding.ASCII.GetString(result.Buffer, 0, result.Buffer.Length),
                Sender = result.RemoteEndPoint
            };
        }
    }
}
