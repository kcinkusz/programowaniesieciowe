﻿namespace Client.Windows
{
    partial class FormClientApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormClientApp));
            this.buttonSendDiscovery = new System.Windows.Forms.Button();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.buttonConnectUDP = new System.Windows.Forms.Button();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonConnectManual = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.IpAddress = new System.Windows.Forms.Label();
            this.PortNumber = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.communicates = new System.Windows.Forms.Label();
            this.sendText = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.buttonSendTest = new System.Windows.Forms.Button();
            this.buttonStartClient = new System.Windows.Forms.Button();
            this.buttonPauseProcess = new System.Windows.Forms.Button();
            this.buttonEndProcess = new System.Windows.Forms.Button();
            this.textBoxClientState = new System.Windows.Forms.TextBox();
            this.acctualClientState = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.toServerConnected = new System.Windows.Forms.TextBox();
            this.buttonSend = new System.Windows.Forms.Button();
            this.clientInformation = new System.Windows.Forms.RichTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.clientCommunicatesAutoScrollingCheckBox = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.buttonSendNumbers = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonSendDiscovery
            // 
            this.buttonSendDiscovery.Location = new System.Drawing.Point(364, 326);
            this.buttonSendDiscovery.Name = "buttonSendDiscovery";
            this.buttonSendDiscovery.Size = new System.Drawing.Size(103, 39);
            this.buttonSendDiscovery.TabIndex = 0;
            this.buttonSendDiscovery.Text = "Pytaj o serwer";
            this.buttonSendDiscovery.UseVisualStyleBackColor = true;
            this.buttonSendDiscovery.Click += new System.EventHandler(this.sendButton_Click);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Enabled = false;
            this.radioButton1.Location = new System.Drawing.Point(13, 342);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(87, 17);
            this.radioButton1.TabIndex = 1;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "DISCOVERY";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Enabled = false;
            this.radioButton2.Location = new System.Drawing.Point(147, 342);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(66, 17);
            this.radioButton2.TabIndex = 2;
            this.radioButton2.Text = "multicast";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Enabled = false;
            this.numericUpDown1.Location = new System.Drawing.Point(238, 342);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown1.TabIndex = 3;
            this.numericUpDown1.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 323);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "treść zapytania";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(146, 323);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "tryb";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(237, 323);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Port";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(12, 447);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(454, 82);
            this.listBox1.TabIndex = 7;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // buttonConnectUDP
            // 
            this.buttonConnectUDP.Location = new System.Drawing.Point(43, 532);
            this.buttonConnectUDP.Name = "buttonConnectUDP";
            this.buttonConnectUDP.Size = new System.Drawing.Size(75, 23);
            this.buttonConnectUDP.TabIndex = 8;
            this.buttonConnectUDP.Text = "Połącz";
            this.buttonConnectUDP.UseVisualStyleBackColor = true;
            this.buttonConnectUDP.Click += new System.EventHandler(this.buttonConnectUDP_Click);
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point(343, 533);
            this.numericUpDown2.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown2.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown2.TabIndex = 9;
            this.numericUpDown2.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(258, 537);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "częstotliwością";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 542);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Logi";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 431);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(174, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Oferty serwerów z numerami portów";
            // 
            // buttonConnectManual
            // 
            this.buttonConnectManual.Location = new System.Drawing.Point(479, 156);
            this.buttonConnectManual.Name = "buttonConnectManual";
            this.buttonConnectManual.Size = new System.Drawing.Size(87, 23);
            this.buttonConnectManual.TabIndex = 14;
            this.buttonConnectManual.Text = "Połącz ręcznie";
            this.buttonConnectManual.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(476, 31);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(299, 20);
            this.textBox1.TabIndex = 15;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(476, 74);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(299, 20);
            this.textBox2.TabIndex = 16;
            // 
            // IpAddress
            // 
            this.IpAddress.AutoSize = true;
            this.IpAddress.Location = new System.Drawing.Point(476, 12);
            this.IpAddress.Name = "IpAddress";
            this.IpAddress.Size = new System.Drawing.Size(46, 13);
            this.IpAddress.TabIndex = 17;
            this.IpAddress.Text = "Adres Ip";
            // 
            // PortNumber
            // 
            this.PortNumber.AutoSize = true;
            this.PortNumber.Location = new System.Drawing.Point(476, 58);
            this.PortNumber.Name = "PortNumber";
            this.PortNumber.Size = new System.Drawing.Size(65, 13);
            this.PortNumber.TabIndex = 18;
            this.PortNumber.Text = "Numer portu";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(16, 129);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(447, 191);
            this.richTextBox1.TabIndex = 19;
            this.richTextBox1.Text = "";
            // 
            // communicates
            // 
            this.communicates.AutoSize = true;
            this.communicates.Location = new System.Drawing.Point(13, 112);
            this.communicates.Name = "communicates";
            this.communicates.Size = new System.Drawing.Size(62, 13);
            this.communicates.TabIndex = 20;
            this.communicates.Text = "Komunikaty";
            // 
            // sendText
            // 
            this.sendText.AutoSize = true;
            this.sendText.Location = new System.Drawing.Point(476, 111);
            this.sendText.Name = "sendText";
            this.sendText.Size = new System.Drawing.Size(90, 13);
            this.sendText.TabIndex = 21;
            this.sendText.Text = "Wyślij wiadomość";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(479, 129);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(296, 20);
            this.textBox3.TabIndex = 22;
            // 
            // buttonSendTest
            // 
            this.buttonSendTest.Location = new System.Drawing.Point(572, 156);
            this.buttonSendTest.Name = "buttonSendTest";
            this.buttonSendTest.Size = new System.Drawing.Size(98, 23);
            this.buttonSendTest.TabIndex = 23;
            this.buttonSendTest.Text = "Testuj";
            this.buttonSendTest.UseVisualStyleBackColor = true;
            this.buttonSendTest.Click += new System.EventHandler(this.buttonSend_Click);
            // 
            // buttonStartClient
            // 
            this.buttonStartClient.Location = new System.Drawing.Point(12, 12);
            this.buttonStartClient.Name = "buttonStartClient";
            this.buttonStartClient.Size = new System.Drawing.Size(138, 23);
            this.buttonStartClient.TabIndex = 24;
            this.buttonStartClient.Text = "Uruchom klienta";
            this.buttonStartClient.UseVisualStyleBackColor = true;
            this.buttonStartClient.Click += new System.EventHandler(this.startClient_Click);
            // 
            // buttonPauseProcess
            // 
            this.buttonPauseProcess.Location = new System.Drawing.Point(12, 41);
            this.buttonPauseProcess.Name = "buttonPauseProcess";
            this.buttonPauseProcess.Size = new System.Drawing.Size(138, 23);
            this.buttonPauseProcess.TabIndex = 25;
            this.buttonPauseProcess.Text = "Zatrzymaj proces";
            this.buttonPauseProcess.UseVisualStyleBackColor = true;
            this.buttonPauseProcess.Click += new System.EventHandler(this.pauseResume_Click);
            // 
            // buttonEndProcess
            // 
            this.buttonEndProcess.Location = new System.Drawing.Point(12, 71);
            this.buttonEndProcess.Name = "buttonEndProcess";
            this.buttonEndProcess.Size = new System.Drawing.Size(138, 23);
            this.buttonEndProcess.TabIndex = 26;
            this.buttonEndProcess.Text = "Zakończ proces klienta";
            this.buttonEndProcess.UseVisualStyleBackColor = true;
            this.buttonEndProcess.Click += new System.EventHandler(this.endClient_Click);
            // 
            // textBoxClientState
            // 
            this.textBoxClientState.Enabled = false;
            this.textBoxClientState.Location = new System.Drawing.Point(182, 28);
            this.textBoxClientState.Name = "textBoxClientState";
            this.textBoxClientState.Size = new System.Drawing.Size(281, 20);
            this.textBoxClientState.TabIndex = 27;
            // 
            // acctualClientState
            // 
            this.acctualClientState.AutoSize = true;
            this.acctualClientState.Location = new System.Drawing.Point(182, 12);
            this.acctualClientState.Name = "acctualClientState";
            this.acctualClientState.Size = new System.Drawing.Size(105, 13);
            this.acctualClientState.TabIndex = 28;
            this.acctualClientState.Text = "Aktualny stan klienta";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(182, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(122, 13);
            this.label7.TabIndex = 29;
            this.label7.Text = "Podłączony pod serwer:";
            // 
            // toServerConnected
            // 
            this.toServerConnected.Enabled = false;
            this.toServerConnected.Location = new System.Drawing.Point(182, 73);
            this.toServerConnected.Name = "toServerConnected";
            this.toServerConnected.Size = new System.Drawing.Size(281, 20);
            this.toServerConnected.TabIndex = 30;
            // 
            // buttonSend
            // 
            this.buttonSend.Location = new System.Drawing.Point(676, 155);
            this.buttonSend.Name = "buttonSend";
            this.buttonSend.Size = new System.Drawing.Size(99, 23);
            this.buttonSend.TabIndex = 31;
            this.buttonSend.Text = "Wyślij";
            this.buttonSend.UseVisualStyleBackColor = true;
            // 
            // clientInformation
            // 
            this.clientInformation.Enabled = false;
            this.clientInformation.Location = new System.Drawing.Point(479, 198);
            this.clientInformation.Name = "clientInformation";
            this.clientInformation.Size = new System.Drawing.Size(299, 416);
            this.clientInformation.TabIndex = 32;
            this.clientInformation.Text = "";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(476, 182);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(101, 13);
            this.label8.TabIndex = 33;
            this.label8.Text = "Informacje klienckie";
            // 
            // clientCommunicatesAutoScrollingCheckBox
            // 
            this.clientCommunicatesAutoScrollingCheckBox.AutoSize = true;
            this.clientCommunicatesAutoScrollingCheckBox.Checked = true;
            this.clientCommunicatesAutoScrollingCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.clientCommunicatesAutoScrollingCheckBox.Location = new System.Drawing.Point(358, 111);
            this.clientCommunicatesAutoScrollingCheckBox.Name = "clientCommunicatesAutoScrollingCheckBox";
            this.clientCommunicatesAutoScrollingCheckBox.Size = new System.Drawing.Size(105, 17);
            this.clientCommunicatesAutoScrollingCheckBox.TabIndex = 35;
            this.clientCommunicatesAutoScrollingCheckBox.Text = "autoscrollowanie";
            this.clientCommunicatesAutoScrollingCheckBox.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 409);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 13);
            this.label9.TabIndex = 36;
            this.label9.Text = "Multicast adres";
            // 
            // textBox4
            // 
            this.textBox4.Enabled = false;
            this.textBox4.Location = new System.Drawing.Point(189, 409);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(274, 20);
            this.textBox4.TabIndex = 37;
            this.textBox4.Text = "127.0.0.1";
            // 
            // richTextBox2
            // 
            this.richTextBox2.Location = new System.Drawing.Point(13, 561);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(450, 53);
            this.richTextBox2.TabIndex = 38;
            this.richTextBox2.Text = "";
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Enabled = false;
            this.radioButton3.Location = new System.Drawing.Point(152, 371);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(206, 17);
            this.radioButton3.TabIndex = 39;
            this.radioButton3.Text = "Automatycznie ponów zapytanie po [s]";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // numericUpDown3
            // 
            this.numericUpDown3.Enabled = false;
            this.numericUpDown3.Location = new System.Drawing.Point(364, 371);
            this.numericUpDown3.Name = "numericUpDown3";
            this.numericUpDown3.Size = new System.Drawing.Size(102, 20);
            this.numericUpDown3.TabIndex = 40;
            this.numericUpDown3.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            // 
            // buttonSendNumbers
            // 
            this.buttonSendNumbers.Location = new System.Drawing.Point(167, 532);
            this.buttonSendNumbers.Name = "buttonSendNumbers";
            this.buttonSendNumbers.Size = new System.Drawing.Size(96, 23);
            this.buttonSendNumbers.TabIndex = 41;
            this.buttonSendNumbers.Text = "Wysyłaj liczby z";
            this.buttonSendNumbers.UseVisualStyleBackColor = true;
            this.buttonSendNumbers.Click += new System.EventHandler(this.buttonSendNumbers_Click);
            // 
            // FormClientApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(790, 623);
            this.Controls.Add(this.buttonSendNumbers);
            this.Controls.Add(this.numericUpDown3);
            this.Controls.Add(this.radioButton3);
            this.Controls.Add(this.richTextBox2);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.clientCommunicatesAutoScrollingCheckBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.clientInformation);
            this.Controls.Add(this.buttonSend);
            this.Controls.Add(this.toServerConnected);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.acctualClientState);
            this.Controls.Add(this.textBoxClientState);
            this.Controls.Add(this.buttonEndProcess);
            this.Controls.Add(this.buttonPauseProcess);
            this.Controls.Add(this.buttonStartClient);
            this.Controls.Add(this.buttonSendTest);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.sendText);
            this.Controls.Add(this.communicates);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.PortNumber);
            this.Controls.Add(this.IpAddress);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.buttonConnectManual);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.numericUpDown2);
            this.Controls.Add(this.buttonConnectUDP);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.buttonSendDiscovery);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormClientApp";
            this.Text = "Aplikacja klient TCP";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonSendDiscovery;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button buttonConnectUDP;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonConnectManual;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label IpAddress;
        private System.Windows.Forms.Label PortNumber;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label communicates;
        private System.Windows.Forms.Label sendText;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button buttonSendTest;
        private System.Windows.Forms.Button buttonStartClient;
        private System.Windows.Forms.Button buttonPauseProcess;
        private System.Windows.Forms.Button buttonEndProcess;
        private System.Windows.Forms.TextBox textBoxClientState;
        private System.Windows.Forms.Label acctualClientState;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox toServerConnected;
        private System.Windows.Forms.Button buttonSend;
        private System.Windows.Forms.RichTextBox clientInformation;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox clientCommunicatesAutoScrollingCheckBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.NumericUpDown numericUpDown3;
        private System.Windows.Forms.Button buttonSendNumbers;
    }
}

