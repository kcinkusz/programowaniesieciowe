﻿using Konrad.Winforms.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Forms;
using TCP.Core;

namespace Client.Windows
{
    public partial class FormClientApp : Form
    {
        private readonly IClient client;

        public FormClientApp()
        {
            InitializeComponent();
            client = TCP.Client.Client.Instance;
            Text += ", identyfikator klienta: " + client.Guid.ToString();

            //TCP.Client.Client.ServerTaskCollection.CollectionChanged += ServerTaskCollection_CollectionChanged;
            TCP.Client.Client.Instance.Name = "Wątek serwerowy";
            TCP.Client.Client.Instance.Request += Client_Request;
            TCP.Client.Client.Instance.PropertyChanged += Client_PropertyChanged;

            setClientButtonEnable(false);

            clientInformation.Text = "Interfejsy sieciowe dostępne na lokalnym komputerze, wraz z numerami IP:" + TCP.Client.Client.Instance.ToString();

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            TCP.Client.Client.Instance.ClientState = ClientState.ClientInitialized;
            ServerOffers.Instance.Request += Instance_Request;
        }

        private void Instance_Request(object sender, string requestMessage, Konrad.Core.TextFormatting messageFormatting)
        {
            if ((sender as string)?.Equals("SERVER OFFER") != null) // :)
                listBox1.BeginInvoke(new Action(() => listBox1.Items.Add(requestMessage)));
        }
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            ServerOffers.Instance.Request -= Instance_Request;
        }

        private void Client_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            textBoxClientState.BeginInvoke(new Action(() => textBoxClientState.Text = e.PropertyName));
        }

        private void Client_Request(object sender, string requestMessage, Konrad.Core.TextFormatting messageFormatting)
        {
            if (string.Equals(Helper.InterruptClientSignal, sender as string)) // Jeśli otrzymamy sygnał interrupt to odblokujemy uruchamianie
                buttonStartClient.BeginInvoke(new Action(() => buttonStartClient.Enabled = true));
            else if (string.Equals(Helper.PauseClientSignal, sender as string))//Jeśli otrzymamy sygnał pausy to odblokujemy ten przycisk
                buttonPauseProcess.BeginInvoke(new Action(() => buttonPauseProcess.Enabled = true));
            else
            {
                ClientState clientState = (sender as IClient).ClientState; //to nie jest tutaj zasadne, ponieważ statet się zmienia na evencie zmiany właściwości
                richTextBox1.BeginInvoke(RichTextBoxExtensions.AppendToRTB(richTextBox1, new Konrad.Winforms.TextFormatting(messageFormatting), requestMessage + "\n", clientCommunicatesAutoScrollingCheckBox.Checked));
                //Tutaj będą wiadomości otrzymane od serwera
                //Właściwie to wiadomości z procesu klienta
            }
        }
        /// <summary>
        /// Pytaj o serwer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sendButton_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();

            string question = radioButton1.Text;
            string mode = radioButton2.Text;
            int port = (int)numericUpDown1.Value;

            SendQuestionForServerOffers(question, mode, port);
        }

        private void SendQuestionForServerOffers(string question, string mode, int port)
        {
            ServerOffers.Instance.SendQuestionForServerOffers(question, mode, port);
        }

        /// <summary>
        /// Testuj połaczenie
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSend_Click(object sender, EventArgs e)
        {
            Task.Factory.StartNew(() => client.Process());
        }

        private void startClient_Click(object sender, EventArgs e)
        {
            TCP.Client.Client.InterruptSignal = false;
            // Task serverTask = Task.Factory.StartNew(() => client.Process());
            setClientButtonEnable(true);
        }

        private void setClientButtonEnable(bool isEnable)
        {
            buttonStartClient.Enabled = !isEnable;
            buttonEndProcess.Enabled = isEnable;
            buttonPauseProcess.Enabled = isEnable;
            buttonPauseProcess.Enabled = isEnable;
            buttonEndProcess.Enabled = isEnable;
            buttonConnectManual.Enabled = isEnable;
            buttonSendTest.Enabled = isEnable;
            buttonSend.Enabled = isEnable;
            //buttonSendDiscovery.Enabled = isEnable;
            buttonConnectUDP.Enabled = isEnable;
        }

        private void endClient_Click(object sender, EventArgs e)
        {
            TCP.Client.Client.InterruptSignal = true;
            //Po wysłaniu sygnału przerwania zawsze trzeba ustawić wznawianie na zatrzymanie
            setClientButtonEnable(false);
            buttonStartClient.Enabled = false;
            buttonPauseProcess.Text = "Zatrzymaj proces";
            TCP.Client.Client.Pause = false;
        }

        private void pauseResume_Click(object sender, EventArgs e)
        {
            buttonPauseProcess.Enabled = false;
            if (buttonPauseProcess.Text == "Zatrzymaj proces")
            {
                buttonPauseProcess.Text = "Wznów proces";
                TCP.Client.Client.Pause = true;
            }
            else
            {
                buttonPauseProcess.Text = "Zatrzymaj proces";
                TCP.Client.Client.Pause = false;
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            buttonConnectUDP.Enabled = true;
            buttonSendNumbers.Enabled = true;
        }

        private void buttonConnectUDP_Click(object sender, EventArgs e)
        {
            string selectedInterfaces = listBox1.SelectedItem as string;
            string[] information = selectedInterfaces.Split(';');
            string[] webInterfaceInformation = information[0].Split(',');
            string IPofSelectedWebInterface = webInterfaceInformation[1];
            IPofSelectedWebInterface = IPofSelectedWebInterface.Replace(" ", string.Empty);
            string PortOfSelectedWebInterface = webInterfaceInformation[2];
            PortOfSelectedWebInterface = PortOfSelectedWebInterface.Replace(" ", string.Empty).Replace(")", string.Empty);
            TCP.Client.Client.InterruptSignal = false;
            IPAddress serverSelectedIP;
            if (IPAddress.TryParse(IPofSelectedWebInterface, out serverSelectedIP))
                ;
            TCP.Client.Client.Instance.address = serverSelectedIP;
            int serverSelectedPort;
            int.TryParse(PortOfSelectedWebInterface, out serverSelectedPort);
            TCP.Client.Client.Instance.portNumber = serverSelectedPort;
            Task serverTask = Task.Factory.StartNew(() => client.Process());
        }

        private void buttonSendNumbers_Click(object sender, EventArgs e)
        {
            if (buttonSendNumbers.Text == "Wysyłaj liczby z")
            {
                TCP.Client.Client.Instance.SendingNumber = new Tuple<bool, int>(true, (int)numericUpDown2.Value);
                buttonSendNumbers.Text = "Zatrzymaj";
            }
            else if (buttonSendNumbers.Text == "Zatrzymaj")
            {
                TCP.Client.Client.InterruptSignal = true;
                buttonSendNumbers.Text = "Wysyłaj liczby z";
            }
        }
    }
}