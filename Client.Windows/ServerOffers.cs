﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TCP.Core;
using UDP.Client;

namespace Client.Windows
{
    public sealed class ServerOffers : Application
    {
        private static ServerOffers instance = null;
        private static readonly object padlock = new object();

        ServerOffers()
        {
            client.Request += Client_Request;
        }
        ~ServerOffers()
        {
            client.Request -= Client_Request;
        }
        public static ServerOffers Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new ServerOffers();

                    }
                    return instance;
                }
            }
        }
        public string MulticastAddress { get; } = "127.0.0.1";
        public UdpUser client { get; } = UdpUser.ConnectTo("127.0.0.1", 7);

        public void SendQuestionForServerOffers(string question, string mode, int port)
        {
            UdpUser.InterruptSignal = false;
            Task.Factory.StartNew(() => client.Process());
        }

        private void Client_Request(object sender, string requestMessage, Konrad.Core.TextFormatting messageFormatting)
        {
            OnRequest(sender, requestMessage, messageFormatting);
        }
    }
}
