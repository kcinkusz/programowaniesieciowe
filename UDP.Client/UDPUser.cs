﻿using System;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TCP.Core;
using UDP.Core;

namespace UDP.Client
{
    public class UdpUser : UdpBase
    {
        private UdpUser() { }

        public static UdpUser ConnectTo(string hostname, int port)
        {
            var connection = new UdpUser();
            connection.Client.Connect(hostname, port);
            return connection;
        }

        public void Send(string message)
        {
            var datagram = Encoding.ASCII.GetBytes(message);
            Client.Send(datagram, datagram.Length);
        }
        public async Task<bool> Process()
        {

            OnRequest(this, "UDP client run", new Konrad.Core.TextFormatting());
            Send("DISCOVER");
            while (!InterruptSignal)
            {
                try
                {
                    Received received = await Receive();
                    IPEndPoint iPEndPoint = received.Sender;
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append(received.Message);
                    stringBuilder.Append(";");
                    stringBuilder.Append(iPEndPoint.Address);
                    stringBuilder.Append(";");
                    stringBuilder.Append(iPEndPoint.Port);
                    
                    //Tutaj powienem otrzymywać oferty serwerów
                    OnRequest("SERVER OFFER", stringBuilder.ToString(), new Konrad.Core.TextFormatting(), false);
                }
                catch (Exception ex)
                {

                }

                await Task.Delay(1200);
            }
            OnRequest(Helper.InterruptClientSignal, null, null);
            return true;
        }

    }
}
