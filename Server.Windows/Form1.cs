﻿using Konrad.Core;
using Konrad.Winforms;
using Konrad.Winforms.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using TCP.Core;
using TCP.Server;
using UDP.Server;

namespace Server.Windows
{
    public partial class Form1 : Form
    {
        private CancellationTokenSource cancelToken = new CancellationTokenSource();
        private TaskScheduler uiScheduler = TaskScheduler.FromCurrentSynchronizationContext();

        private readonly IServer server;

        public Form1()
        {
            InitializeComponent();
            server = TCP.Server.Server.Instance; // Jeżeli korzystamy z singletona to ta zmienna i interfejs nie spełniają swojej funkcjoi
            Text += ", identyfikator serwera: " + server.Guid.ToString();

            TCP.Server.Server.Instance.ServerTaskCollection.CollectionChanged += ServerTaskCollection_CollectionChanged;
            TCP.Server.Server.Instance.Name = "Wątek serwerowy";
            TCP.Server.Server.Instance.Request += ServerBasic_Request;
            TCP.Server.Server.Instance.PropertyChanged += Server_PropertyChanged;

            setServerButtonEnable(false);

            IEnumerable<Tuple<string, IPAddress>> addresses = Helper.GetIpOfAllNetworkInterfaces().ToList();

            if (addresses.Count() == 0)
            {
                buttonStartServer.Enabled = false;
                serverInformation.Text = "Brak interfejsów sieciowych na komputerze, nie możemy uruchomić serwera";
                return;
            }
            serverInformation.Text = "Interfejsy sieciowe dostępne na komputerze, wraz z numerami IP:" + TCP.Server.Server.Instance.ToString();


            listBox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            listBox1.MeasureItem += lst_MeasureItem;
            listBox1.DrawItem += lst_DrawItem;

            this.listBox1.DataSource = addresses.ToList();

            listBox1.HorizontalScrollbar = true;

            this.listBox1.DisplayMember = "Item1";
            this.listBox1.ValueMember = "Item2";

        }
        private UdpListener udpListener = null;
        /// <summary>
        /// Wrapowanie na listboxie ściągnięte z:
        /// https://stackoverflow.com/questions/17613613/winforms-dotnet-listbox-items-to-word-wrap-if-content-string-width-is-bigger-tha#17640123
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lst_MeasureItem(object sender, MeasureItemEventArgs e)
        {
            e.ItemHeight = (int)e.Graphics.MeasureString(listBox1.Items[e.Index].ToString(), listBox1.Font, listBox1.Width).Height;
        }

        private void lst_DrawItem(object sender, DrawItemEventArgs e)
        {
            e.DrawBackground();
            e.DrawFocusRectangle();
            e.Graphics.DrawString(listBox1.Items[e.Index].ToString(), e.Font, new System.Drawing.SolidBrush(e.ForeColor), e.Bounds);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            TCP.Server.Server.Instance.ServerState = ServerState.Initialized;
        }

        private void ServerTaskCollection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            IList<Task> taskList = sender as IList<Task>;

            foreach (var x in e.NewItems)
            {
                Task tmp = x as Task;
                textBoxServerState.BeginInvoke(new Action(() => waitingList.Text += $"{tmp.Id}:{tmp.Status}"));
            }

            //foreach (var y in e.OldItems)
            //{
            //    textBoxServerState.BeginInvoke(new Action(() => clientDone.Text = y.ToString()));
            //}
            //if (e.Action == NotifyCollectionChangedAction.Move)
            //{
            //    //do something
            //}
        }

        private void Server_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            textBoxServerState.BeginInvoke(new Action(() => textBoxServerState.Text = e.PropertyName));
        }

        /// <summary>
        /// https://stackoverflow.com/questions/463299/how-do-i-make-a-textbox-that-only-accepts-numbers#463335
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            //if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            //{
            //    e.Handled = true;
            //}
        }

        private void startServer_Click(object sender, EventArgs e)
        {
            int portNumber = Helper.DEFAULT_PORT_NO;
            if (randomPortButton.Checked)
            {
                if (string.IsNullOrEmpty(textBoxRandomPortBegin.Text))
                {
                    textBoxRandomPortBegin.Text = "0";
                }
                if (string.IsNullOrEmpty(textBoxRandomPortEnd.Text))
                {
                    textBoxRandomPortEnd.Text = "10000";
                }
                int begin;
                bool beginParseSuccess = int.TryParse(textBoxRandomPortBegin.Text, out begin);
                int end;
                bool endParseSuccess = int.TryParse(textBoxRandomPortEnd.Text, out end);
                if (!beginParseSuccess || !endParseSuccess)
                {
                    begin = 0;
                    end = 10000;
                    BeginInvoke(RichTextBoxExtensions.AppendToRTB(serverCommunicatesRichTextBox, new Konrad.Winforms.TextFormatting(),
                        "Błędne wartości, ustawiono zakres losowy na domyślny od 0 do 10000", serverCommunicatesAutoScrollingCheckBox.Checked));
                }
                else if (begin > end)
                {
                    textBoxRandomPortEnd.Text = "0";
                    textBoxRandomPortBegin.Text = "10000";
                    BeginInvoke(RichTextBoxExtensions.AppendToRTB(serverCommunicatesRichTextBox, new Konrad.Winforms.TextFormatting(), "Wpisano zły zakres portów losowych, ustawiono zakres losowy na domyślny od 0 do 10000", serverCommunicatesAutoScrollingCheckBox.Checked));
                }
                Random random = RandomExtended.Build(SeedGeneratorMethod.Classic);
                portNumber = random.Next(begin, end);
            }
            else if (radioButton3.Checked)
            {
                if (string.IsNullOrEmpty(textBox1.Text))
                    textBox1.Text = Helper.DEFAULT_PORT_NO.ToString();
                int portTemp;
                if (int.TryParse(textBox1.Text, out portTemp))
                    portNumber = portTemp;
            }
            if (listBox1.Items.Count > 0)
            {
                //Jak są zaznaczone wszystkie, to bierze tylko pierwszy
                //Na wybranym interfejsie:
                Tuple<string, IPAddress> selectedNetworkInterface = listBox1.SelectedItem as Tuple<string, IPAddress>;

                TCP.Server.Server.Instance.address = selectedNetworkInterface.Item2;

                TCP.Server.Server.Instance.endPoint = new IPEndPoint(TCP.Server.Server.Instance.address, portNumber);

                //muszę utworzyć sobie tyle zadań ile jest interfejsów sieciowych
            }
            TCP.Server.Server.InterruptSignal = false;
            Task serverTask = Task.Factory.StartNew(() => server.Process());

            setStartingServerPropertiesEnable(false);
            setServerButtonEnable(true);

            if (udpDiscoverSignal.Checked)
            {
                udpListener = new UdpListener(7);
                udpListener.Request += UdpListener_Request;
                bool networkUp = System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable();
                System.Net.NetworkInformation.NetworkInterface[] networkCards = System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces();
                foreach (var networkInterface in (listBox1.SelectedItems as ListBox.SelectedObjectCollection))
                {
                    Tuple<string, IPAddress> nI = networkInterface as Tuple<string, IPAddress>;
                    udpListener.networkInterfacesRun.Add(new Tuple<string, IPAddress, int>(nI.Item1,nI.Item2, portNumber));
                }
                Task.Factory.StartNew(async () => udpListener.Process());

                BeginInvoke(RichTextBoxExtensions.AppendToRTB(serverCommunicatesRichTextBox, new Konrad.Winforms.TextFormatting(),
                    "Uruchomiono nasłuch UDP, wysyłam oferty z wybranymi interfejsami", serverCommunicatesAutoScrollingCheckBox.Checked));
            }
            else
            {
                udpListener = null;
            }
        }

        private void UdpListener_Request(object sender, string requestMessage, Konrad.Core.TextFormatting messageFormatting)
        {
            if (sender is string && (sender as string) == Helper.InterruptServerSignal)
            {
                udpDiscoverSignal.Enabled = true;
                udpDiscoverSignal.Checked = false;
                udpListener = null;
            }
            else
            {
                BeginInvoke(RichTextBoxExtensions.AppendToRTB(serverCommunicatesRichTextBox, new Konrad.Winforms.TextFormatting(messageFormatting), requestMessage, clientCommunicatesAutoScrollingCheckBox.Checked));
            }
        }
        private void setStartingServerPropertiesEnableAsync(bool isEnabled)
        {
            randomPortButton.BeginInvoke(new Action(()=> randomPortButton.Enabled = isEnabled));
            if (randomPortButton.Checked && isEnabled)
            {
                textBoxRandomPortEnd.BeginInvoke(new Action(() => textBoxRandomPortEnd.Enabled = isEnabled));
                textBoxRandomPortBegin.BeginInvoke(new Action(() => textBoxRandomPortBegin.Enabled = isEnabled));
            }
            else
            {
                textBoxRandomPortEnd.BeginInvoke(new Action(() => textBoxRandomPortEnd.Enabled = false));
                textBoxRandomPortBegin.BeginInvoke(new Action(() => textBoxRandomPortBegin.Enabled = false));
            }
            radioButton1.BeginInvoke(new Action(() => radioButton1.Enabled = isEnabled));
            radioButton3.BeginInvoke(new Action(() => radioButton3.Enabled = isEnabled));
            if (radioButton3.Checked && isEnabled)
            {
                textBox1.BeginInvoke(new Action(() => textBox1.Enabled = isEnabled));
            }
            else
            {
                textBox1.BeginInvoke(new Action(() => textBox1.Enabled = false));
            }

            listBox1.BeginInvoke(new Action(() => listBox1.Enabled = isEnabled));
            checkBox1.BeginInvoke(new Action(() => checkBox1.Enabled = isEnabled));
            udpDiscoverSignal.BeginInvoke(new Action(() => udpDiscoverSignal.Enabled = isEnabled));

        }
        private void setStartingServerPropertiesEnable(bool isEnabled)
        {

            randomPortButton.Enabled = isEnabled;
            if (randomPortButton.Checked && isEnabled)
            {
                textBoxRandomPortEnd.Enabled = isEnabled;
                textBoxRandomPortBegin.Enabled = isEnabled;
            }
            else
            {
                textBoxRandomPortEnd.Enabled = false;
                textBoxRandomPortBegin.Enabled = false;
            }
            radioButton1.Enabled = isEnabled;
            radioButton3.Enabled = isEnabled;
            if (radioButton3.Checked && isEnabled)
            {
                textBox1.Enabled = isEnabled;
            }
            else
            {
                textBox1.Enabled = false;
            }

            listBox1.Enabled = isEnabled;
            checkBox1.Enabled = isEnabled;
            udpDiscoverSignal.Enabled = isEnabled;

        }

        private void ServerBasic_Request(object sender, string requestMessage, Konrad.Core.TextFormatting requestMessageFormatting)
        {
            string text = (requestMessage == Helper.InterruptServerSignal) ? "Odebrano sygnał przerwania pracy serwera, dziękujemy za korzystanie z serwera\n" : $"{requestMessage}{Helper.NewLine}";
            if (string.Equals(Helper.InterruptServerSignal, sender as string)) // Jeśli otrzymamy sygnał interrupt to odblokujemy uruchamianie
            {
                buttonStartServer.BeginInvoke(new Action(() => buttonStartServer.Enabled = true));
                setStartingServerPropertiesEnableAsync(true);
                setServerButtonEnableAsync(false);
            }
            else if (string.Equals(Helper.PauseServerSignal, sender as string))//Jeśli otrzymamy sygnał pausy to odblokujemy ten przycisk
                buttonPauseProcess.BeginInvoke(new Action(() => buttonPauseProcess.Enabled = true));
            else if (sender is ServerRunner)
            {
                if (requestMessage == Helper.RunnerStartReceivingSignal)
                    BeginInvoke(RichTextBoxExtensions.AppendToRTB(serverCommunicatesRichTextBox, new Konrad.Winforms.TextFormatting { Color = System.Drawing.Color.Green }, "Klient nawiązał połączenie i wysyła dane\n", serverCommunicatesAutoScrollingCheckBox.Checked));
                //To są wiadomości od klienta
                BeginInvoke(RichTextBoxExtensions.AppendToRTB(richTextBoxClientMessages, new Konrad.Winforms.TextFormatting(requestMessageFormatting), text, clientCommunicatesAutoScrollingCheckBox.Checked));
                //Poniżej powinna być informacja do serwera, do okienka serwerowego, jak niżej
            }
            else if (sender is TCP.Server.Server)
            {
                BeginInvoke(RichTextBoxExtensions.AppendToRTB(serverCommunicatesRichTextBox, new Konrad.Winforms.TextFormatting(requestMessageFormatting), text, serverCommunicatesAutoScrollingCheckBox.Checked));
            }
            else if (sender is ReceivedClientInformation)
            {
                BeginInvoke(new Action(() =>
                {
                    connectedClient.ResetText();
                    connectedClient.AppendText(requestMessage);
                }));
            }
        }



        private void stopProcess_Click(object sender, EventArgs e)
        {
            TCP.Server.Server.InterruptSignal = true;
            setServerButtonEnable(false);
            buttonStartServer.Enabled = false;
            buttonPauseProcess.Text = "Zatrzymaj proces";
            TCP.Server.Server.Pause = false;

            if (udpListener != null)
            {
                udpListener.InterruptSignal = true;
                udpDiscoverSignal.Enabled = false;
            }
        }
        private void setServerButtonEnableAsync(bool isEnable)
        {
            buttonStartServer.BeginInvoke(new Action(() => buttonStartServer.Enabled = !isEnable));
            buttonEndProcess.BeginInvoke(new Action(() => buttonEndProcess.Enabled = isEnable));
            buttonPauseProcess.BeginInvoke(new Action(() => buttonPauseProcess.Enabled = isEnable));
        }

        private void setServerButtonEnable(bool isEnable)
        {
            buttonStartServer.Enabled = !isEnable;
            buttonEndProcess.Enabled = isEnable;
            buttonPauseProcess.Enabled = isEnable;
        }

        private void pauseResume_Click(object sender, EventArgs e)
        {
            buttonPauseProcess.Enabled = false;
            if (buttonPauseProcess.Text == "Zatrzymaj proces")
            {
                TCP.Server.Server.Pause = true;
                buttonPauseProcess.Text = "Wznów proces";
            }
            else
            {
                TCP.Server.Server.Pause = false;
                buttonPauseProcess.Text = "Zatrzymaj proces";
            }
        }

        private void checkBoxAllInterface(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                randomPortButton.Checked = true;
                radioButton1.Checked = false;
                radioButton1.Enabled = false;
                radioButton3.Checked = false;
                radioButton3.Enabled = false;
                textBox1.Enabled = false;
                textBox1.Text = string.Empty;
            }
            else
            {
                radioButton1.Enabled = true;
                radioButton3.Enabled = true;
            }
        }

        private void radioButtonClick(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                randomPortButton.Checked = true;
                radioButton1.Checked = false;
                radioButton3.Checked = false;

                textBox1.Enabled = false;
                textBox1.Text = string.Empty;

                textBoxRandomPortEnd.Enabled = false;
                textBoxRandomPortEnd.Text = string.Empty;

                textBoxRandomPortBegin.Enabled = false;
                textBoxRandomPortBegin.Text = string.Empty;
            }
            else if (radioButton1.Checked && (sender as RadioButton).Name == "radioButton1")
            {
                textBoxRandomPortEnd.Enabled = false;
                textBoxRandomPortEnd.Text = string.Empty;

                textBoxRandomPortBegin.Enabled = false;
                textBoxRandomPortBegin.Text = string.Empty;

                //klikneliśmy na zaznaczony
                textBox1.Enabled = false;
                textBox1.Text = string.Empty;
            }
            else if (!radioButton1.Checked && (sender as RadioButton).Name == "radioButton1")
            {
                //klikneliśmy na odznaczony
                radioButton1.Checked = true;
                randomPortButton.Checked = false;
                radioButton3.Checked = false;

                textBox1.Enabled = false;
                textBox1.Text = string.Empty;

                textBoxRandomPortEnd.Enabled = false;
                textBoxRandomPortEnd.Text = string.Empty;

                textBoxRandomPortBegin.Enabled = false;
                textBoxRandomPortBegin.Text = string.Empty;
            }
            else if (randomPortButton.Checked && (sender as RadioButton).Name == "radioButton2")
            {

                textBoxRandomPortBegin.Enabled = true;
                textBoxRandomPortBegin.Text = string.Empty;

                textBoxRandomPortEnd.Enabled = true;
                textBoxRandomPortEnd.Text = string.Empty;

                //klikneliśmy na zaznaczony
                textBox1.Enabled = false;
                textBox1.Text = string.Empty;
            }
            else if (!randomPortButton.Checked && (sender as RadioButton).Name == "radioButton2")
            {
                //klikneliśmy na odznaczony
                randomPortButton.Checked = true;
                radioButton1.Checked = false;
                radioButton3.Checked = false;

                textBox1.Enabled = false;
                textBox1.Text = string.Empty;

                textBoxRandomPortBegin.Enabled = false;
                textBoxRandomPortBegin.Text = string.Empty;

                textBoxRandomPortEnd.Enabled = false;
                textBoxRandomPortEnd.Text = string.Empty;
            }
            else if (radioButton3.Checked && (sender as RadioButton).Name == "radioButton3")
            {
                //klikneliśmy na zaznaczony
                textBox1.Enabled = true;
                textBox1.Text = string.Empty;

                textBoxRandomPortBegin.Enabled = false;
                textBoxRandomPortBegin.Text = string.Empty;

                textBoxRandomPortEnd.Enabled = false;
                textBoxRandomPortEnd.Text = string.Empty;

            }
            else if (!radioButton3.Checked && (sender as RadioButton).Name == "radioButton3")
            {
                //klikneliśmy na odznaczony
                radioButton3.Checked = true;
                radioButton1.Checked = false;
                randomPortButton.Checked = false;

                textBox1.Enabled = false;
                textBox1.Text = string.Empty;

                textBoxRandomPortEnd.Enabled = false;
                textBoxRandomPortEnd.Text = string.Empty;

                textBoxRandomPortBegin.Enabled = false;
                textBoxRandomPortBegin.Text = string.Empty;
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if ((sender as CheckBox).CheckState == CheckState.Checked)
            {
                //listBox1.SelectionMode = SelectionMode.MultiExtended;
                for (int i = 0; i < listBox1.Items.Count; i++)
                {
                    listBox1.SetSelected(i, true);
                }
                listBox1.Enabled = false;
            }
            else
            {
                for (int i = 1; i < listBox1.Items.Count; i++)
                {
                    listBox1.SetSelected(i, false);
                }
                //listBox1.SelectionMode = SelectionMode.One;
                listBox1.Enabled = true;
            }
        }

        private void interwałPracyToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void randomPortButton_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void udpDiscoverSignal_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void connectedClient_TextChanged(object sender, EventArgs e)
        {

        }
    }
}