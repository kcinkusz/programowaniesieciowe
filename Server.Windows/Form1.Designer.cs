﻿namespace Server.Windows
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.serverCommunicatesRichTextBox = new System.Windows.Forms.RichTextBox();
            this.waitingList = new System.Windows.Forms.RichTextBox();
            this.connectedClient = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonStartServer = new System.Windows.Forms.Button();
            this.serverInformation = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonPauseProcess = new System.Windows.Forms.Button();
            this.buttonEndProcess = new System.Windows.Forms.Button();
            this.serverActualState = new System.Windows.Forms.Label();
            this.textBoxServerState = new System.Windows.Forms.TextBox();
            this.clientDone = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.richTextBoxClientMessages = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.randomPortButton = new System.Windows.Forms.RadioButton();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.udpDiscoverSignal = new System.Windows.Forms.CheckBox();
            this.serverCommunicatesAutoScrollingCheckBox = new System.Windows.Forms.CheckBox();
            this.clientCommunicatesAutoScrollingCheckBox = new System.Windows.Forms.CheckBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBoxRandomPortBegin = new System.Windows.Forms.TextBox();
            this.textBoxRandomPortEnd = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.plikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.właściwościToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.interwałPracyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oAutorzeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // serverCommunicatesRichTextBox
            // 
            this.serverCommunicatesRichTextBox.Location = new System.Drawing.Point(29, 475);
            this.serverCommunicatesRichTextBox.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.serverCommunicatesRichTextBox.Name = "serverCommunicatesRichTextBox";
            this.serverCommunicatesRichTextBox.Size = new System.Drawing.Size(727, 572);
            this.serverCommunicatesRichTextBox.TabIndex = 0;
            this.serverCommunicatesRichTextBox.Text = "";
            // 
            // waitingList
            // 
            this.waitingList.Enabled = false;
            this.waitingList.Location = new System.Drawing.Point(27, 258);
            this.waitingList.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.waitingList.Name = "waitingList";
            this.waitingList.Size = new System.Drawing.Size(729, 166);
            this.waitingList.TabIndex = 1;
            this.waitingList.Text = "";
            // 
            // connectedClient
            // 
            this.connectedClient.Location = new System.Drawing.Point(341, 64);
            this.connectedClient.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.connectedClient.Name = "connectedClient";
            this.connectedClient.Size = new System.Drawing.Size(1164, 38);
            this.connectedClient.TabIndex = 2;
            this.connectedClient.TextChanged += new System.EventHandler(this.connectedClient_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 81);
            this.label1.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(238, 32);
            this.label1.TabIndex = 3;
            this.label1.Text = "Podłączony klient";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 196);
            this.label2.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(162, 32);
            this.label2.TabIndex = 4;
            this.label2.Text = "Oczekujący";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 436);
            this.label3.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(307, 32);
            this.label3.TabIndex = 5;
            this.label3.Text = "Komunikaty serwerowe";
            // 
            // buttonStartServer
            // 
            this.buttonStartServer.Location = new System.Drawing.Point(1528, 57);
            this.buttonStartServer.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.buttonStartServer.Name = "buttonStartServer";
            this.buttonStartServer.Size = new System.Drawing.Size(576, 55);
            this.buttonStartServer.TabIndex = 6;
            this.buttonStartServer.Text = "Uruchom serwer";
            this.buttonStartServer.UseVisualStyleBackColor = true;
            this.buttonStartServer.Click += new System.EventHandler(this.startServer_Click);
            // 
            // serverInformation
            // 
            this.serverInformation.Enabled = false;
            this.serverInformation.Location = new System.Drawing.Point(1528, 303);
            this.serverInformation.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.serverInformation.Name = "serverInformation";
            this.serverInformation.Size = new System.Drawing.Size(569, 743);
            this.serverInformation.TabIndex = 7;
            this.serverInformation.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1528, 265);
            this.label4.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(298, 32);
            this.label4.TabIndex = 8;
            this.label4.Text = "Informacje serwerowe:";
            // 
            // buttonPauseProcess
            // 
            this.buttonPauseProcess.Enabled = false;
            this.buttonPauseProcess.Location = new System.Drawing.Point(1528, 119);
            this.buttonPauseProcess.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.buttonPauseProcess.Name = "buttonPauseProcess";
            this.buttonPauseProcess.Size = new System.Drawing.Size(576, 55);
            this.buttonPauseProcess.TabIndex = 9;
            this.buttonPauseProcess.Text = "Zatrzymaj proces";
            this.buttonPauseProcess.UseVisualStyleBackColor = true;
            this.buttonPauseProcess.Click += new System.EventHandler(this.pauseResume_Click);
            // 
            // buttonEndProcess
            // 
            this.buttonEndProcess.Enabled = false;
            this.buttonEndProcess.Location = new System.Drawing.Point(1528, 184);
            this.buttonEndProcess.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.buttonEndProcess.Name = "buttonEndProcess";
            this.buttonEndProcess.Size = new System.Drawing.Size(576, 55);
            this.buttonEndProcess.TabIndex = 10;
            this.buttonEndProcess.Text = "Zakończ proces serwera";
            this.buttonEndProcess.UseVisualStyleBackColor = true;
            this.buttonEndProcess.Click += new System.EventHandler(this.stopProcess_Click);
            // 
            // serverActualState
            // 
            this.serverActualState.AutoSize = true;
            this.serverActualState.Location = new System.Drawing.Point(21, 126);
            this.serverActualState.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.serverActualState.Name = "serverActualState";
            this.serverActualState.Size = new System.Drawing.Size(301, 32);
            this.serverActualState.TabIndex = 11;
            this.serverActualState.Text = "Aktualny stan serwera:";
            // 
            // textBoxServerState
            // 
            this.textBoxServerState.Enabled = false;
            this.textBoxServerState.Location = new System.Drawing.Point(341, 119);
            this.textBoxServerState.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.textBoxServerState.Name = "textBoxServerState";
            this.textBoxServerState.Size = new System.Drawing.Size(668, 38);
            this.textBoxServerState.TabIndex = 12;
            // 
            // clientDone
            // 
            this.clientDone.Enabled = false;
            this.clientDone.Location = new System.Drawing.Point(779, 258);
            this.clientDone.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.clientDone.Name = "clientDone";
            this.clientDone.Size = new System.Drawing.Size(727, 166);
            this.clientDone.TabIndex = 13;
            this.clientDone.Text = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(773, 219);
            this.label5.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(143, 32);
            this.label5.TabIndex = 14;
            this.label5.Text = "Obsłużeni";
            // 
            // richTextBoxClientMessages
            // 
            this.richTextBoxClientMessages.Location = new System.Drawing.Point(779, 477);
            this.richTextBoxClientMessages.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.richTextBoxClientMessages.Name = "richTextBoxClientMessages";
            this.richTextBoxClientMessages.Size = new System.Drawing.Size(727, 572);
            this.richTextBoxClientMessages.TabIndex = 15;
            this.richTextBoxClientMessages.Text = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(771, 436);
            this.label6.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(300, 32);
            this.label6.TabIndex = 16;
            this.label6.Text = "Wiadomości od klienta";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(2123, 69);
            this.radioButton1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(172, 36);
            this.radioButton1.TabIndex = 17;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "port 5000";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.Click += new System.EventHandler(this.radioButtonClick);
            // 
            // randomPortButton
            // 
            this.randomPortButton.AutoSize = true;
            this.randomPortButton.Location = new System.Drawing.Point(2123, 134);
            this.randomPortButton.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.randomPortButton.Name = "randomPortButton";
            this.randomPortButton.Size = new System.Drawing.Size(195, 36);
            this.randomPortButton.TabIndex = 18;
            this.randomPortButton.Text = "port losowy";
            this.randomPortButton.UseVisualStyleBackColor = true;
            this.randomPortButton.CheckedChanged += new System.EventHandler(this.randomPortButton_CheckedChanged);
            this.randomPortButton.Click += new System.EventHandler(this.radioButtonClick);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(2123, 956);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(514, 36);
            this.checkBox1.TabIndex = 19;
            this.checkBox1.Text = "Nasłuchuj na wszystkich interfejsach";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            this.checkBox1.Click += new System.EventHandler(this.checkBoxAllInterface);
            // 
            // udpDiscoverSignal
            // 
            this.udpDiscoverSignal.AutoSize = true;
            this.udpDiscoverSignal.Checked = true;
            this.udpDiscoverSignal.CheckState = System.Windows.Forms.CheckState.Checked;
            this.udpDiscoverSignal.Location = new System.Drawing.Point(2123, 1011);
            this.udpDiscoverSignal.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.udpDiscoverSignal.Name = "udpDiscoverSignal";
            this.udpDiscoverSignal.Size = new System.Drawing.Size(346, 36);
            this.udpDiscoverSignal.TabIndex = 20;
            this.udpDiscoverSignal.Text = "DISCOVER na porcie 7";
            this.udpDiscoverSignal.UseVisualStyleBackColor = true;
            this.udpDiscoverSignal.CheckedChanged += new System.EventHandler(this.udpDiscoverSignal_CheckedChanged);
            // 
            // serverCommunicatesAutoScrollingCheckBox
            // 
            this.serverCommunicatesAutoScrollingCheckBox.AutoSize = true;
            this.serverCommunicatesAutoScrollingCheckBox.Checked = true;
            this.serverCommunicatesAutoScrollingCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.serverCommunicatesAutoScrollingCheckBox.Location = new System.Drawing.Point(341, 427);
            this.serverCommunicatesAutoScrollingCheckBox.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.serverCommunicatesAutoScrollingCheckBox.Name = "serverCommunicatesAutoScrollingCheckBox";
            this.serverCommunicatesAutoScrollingCheckBox.Size = new System.Drawing.Size(267, 36);
            this.serverCommunicatesAutoScrollingCheckBox.TabIndex = 21;
            this.serverCommunicatesAutoScrollingCheckBox.Text = "autoscrollowanie";
            this.serverCommunicatesAutoScrollingCheckBox.UseVisualStyleBackColor = true;
            // 
            // clientCommunicatesAutoScrollingCheckBox
            // 
            this.clientCommunicatesAutoScrollingCheckBox.AutoSize = true;
            this.clientCommunicatesAutoScrollingCheckBox.Checked = true;
            this.clientCommunicatesAutoScrollingCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.clientCommunicatesAutoScrollingCheckBox.Location = new System.Drawing.Point(1091, 427);
            this.clientCommunicatesAutoScrollingCheckBox.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.clientCommunicatesAutoScrollingCheckBox.Name = "clientCommunicatesAutoScrollingCheckBox";
            this.clientCommunicatesAutoScrollingCheckBox.Size = new System.Drawing.Size(267, 36);
            this.clientCommunicatesAutoScrollingCheckBox.TabIndex = 22;
            this.clientCommunicatesAutoScrollingCheckBox.Text = "autoscrollowanie";
            this.clientCommunicatesAutoScrollingCheckBox.UseVisualStyleBackColor = true;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 31;
            this.listBox1.Location = new System.Drawing.Point(2120, 591);
            this.listBox1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.listBox1.Name = "listBox1";
            this.listBox1.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBox1.Size = new System.Drawing.Size(537, 345);
            this.listBox1.TabIndex = 23;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(2120, 198);
            this.radioButton3.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(308, 36);
            this.radioButton3.TabIndex = 24;
            this.radioButton3.Text = "port niestandardowy";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            this.radioButton3.Click += new System.EventHandler(this.radioButtonClick);
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(2467, 191);
            this.textBox1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.textBox1.MaxLength = 10;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(193, 38);
            this.textBox1.TabIndex = 25;
            this.textBox1.Text = "0";
            this.textBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // textBoxRandomPortBegin
            // 
            this.textBoxRandomPortBegin.Enabled = false;
            this.textBoxRandomPortBegin.Location = new System.Drawing.Point(2419, 126);
            this.textBoxRandomPortBegin.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.textBoxRandomPortBegin.MaxLength = 5;
            this.textBoxRandomPortBegin.Name = "textBoxRandomPortBegin";
            this.textBoxRandomPortBegin.Size = new System.Drawing.Size(193, 38);
            this.textBoxRandomPortBegin.TabIndex = 26;
            this.textBoxRandomPortBegin.Text = "0";
            this.textBoxRandomPortBegin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // textBoxRandomPortEnd
            // 
            this.textBoxRandomPortEnd.Enabled = false;
            this.textBoxRandomPortEnd.Location = new System.Drawing.Point(2701, 126);
            this.textBoxRandomPortEnd.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.textBoxRandomPortEnd.MaxLength = 5;
            this.textBoxRandomPortEnd.Name = "textBoxRandomPortEnd";
            this.textBoxRandomPortEnd.Size = new System.Drawing.Size(193, 38);
            this.textBoxRandomPortEnd.TabIndex = 27;
            this.textBoxRandomPortEnd.Text = "10000";
            this.textBoxRandomPortEnd.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(2347, 136);
            this.label7.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 32);
            this.label7.TabIndex = 28;
            this.label7.Text = "od";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(2635, 136);
            this.label8.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 32);
            this.label8.TabIndex = 29;
            this.label8.Text = "do";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.plikToolStripMenuItem,
            this.właściwościToolStripMenuItem,
            this.oAutorzeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(16, 5, 0, 5);
            this.menuStrip1.Size = new System.Drawing.Size(2981, 55);
            this.menuStrip1.TabIndex = 30;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // plikToolStripMenuItem
            // 
            this.plikToolStripMenuItem.Name = "plikToolStripMenuItem";
            this.plikToolStripMenuItem.Size = new System.Drawing.Size(76, 45);
            this.plikToolStripMenuItem.Text = "Plik";
            // 
            // właściwościToolStripMenuItem
            // 
            this.właściwościToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.interwałPracyToolStripMenuItem});
            this.właściwościToolStripMenuItem.Name = "właściwościToolStripMenuItem";
            this.właściwościToolStripMenuItem.Size = new System.Drawing.Size(189, 45);
            this.właściwościToolStripMenuItem.Text = "Właściwości";
            // 
            // interwałPracyToolStripMenuItem
            // 
            this.interwałPracyToolStripMenuItem.Name = "interwałPracyToolStripMenuItem";
            this.interwałPracyToolStripMenuItem.Size = new System.Drawing.Size(318, 46);
            this.interwałPracyToolStripMenuItem.Text = "Interwał pracy";
            this.interwałPracyToolStripMenuItem.Click += new System.EventHandler(this.interwałPracyToolStripMenuItem_Click);
            // 
            // oAutorzeToolStripMenuItem
            // 
            this.oAutorzeToolStripMenuItem.Name = "oAutorzeToolStripMenuItem";
            this.oAutorzeToolStripMenuItem.Size = new System.Drawing.Size(161, 45);
            this.oAutorzeToolStripMenuItem.Text = "O autorze";
            // 
            // textBox2
            // 
            this.textBox2.Enabled = false;
            this.textBox2.Location = new System.Drawing.Point(2123, 303);
            this.textBox2.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(529, 269);
            this.textBox2.TabIndex = 31;
            this.textBox2.Text = "Komunikaty: Wybierz wiele, ponieważ niektóre interfejsy sieciowe są nieobsługiwan" +
    "e, funkcja nasłuchuj na wszystkich interfejsach może generować wyjątek.";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2981, 1102);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBoxRandomPortEnd);
            this.Controls.Add(this.textBoxRandomPortBegin);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.radioButton3);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.clientCommunicatesAutoScrollingCheckBox);
            this.Controls.Add(this.serverCommunicatesAutoScrollingCheckBox);
            this.Controls.Add(this.udpDiscoverSignal);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.randomPortButton);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.richTextBoxClientMessages);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.clientDone);
            this.Controls.Add(this.textBoxServerState);
            this.Controls.Add(this.serverActualState);
            this.Controls.Add(this.buttonEndProcess);
            this.Controls.Add(this.buttonPauseProcess);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.serverInformation);
            this.Controls.Add(this.buttonStartServer);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.connectedClient);
            this.Controls.Add(this.waitingList);
            this.Controls.Add(this.serverCommunicatesRichTextBox);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.MaximumSize = new System.Drawing.Size(3013, 1190);
            this.MinimumSize = new System.Drawing.Size(3013, 1190);
            this.Name = "Form1";
            this.Text = "Serwer TCP";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox serverCommunicatesRichTextBox;
        private System.Windows.Forms.RichTextBox waitingList;
        private System.Windows.Forms.TextBox connectedClient;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonStartServer;
        private System.Windows.Forms.RichTextBox serverInformation;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonPauseProcess;
        private System.Windows.Forms.Button buttonEndProcess;
        private System.Windows.Forms.Label serverActualState;
        private System.Windows.Forms.TextBox textBoxServerState;
        private System.Windows.Forms.RichTextBox clientDone;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox richTextBoxClientMessages;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton randomPortButton;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox udpDiscoverSignal;
        private System.Windows.Forms.CheckBox serverCommunicatesAutoScrollingCheckBox;
        private System.Windows.Forms.CheckBox clientCommunicatesAutoScrollingCheckBox;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBoxRandomPortBegin;
        private System.Windows.Forms.TextBox textBoxRandomPortEnd;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem plikToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem właściwościToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem interwałPracyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oAutorzeToolStripMenuItem;
        private System.Windows.Forms.TextBox textBox2;
    }
}

