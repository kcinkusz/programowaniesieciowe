﻿using System.Threading.Tasks;

namespace Server.Console
{
    /// <summary>
    /// Interfejs opisujący klasy, którę mają stałą prace do wykonaniam
    /// </summary>
    public interface IWorkflow
    {
        Task<bool> Process();
    }
}