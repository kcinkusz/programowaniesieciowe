﻿using Client.Console;
using Server.Console;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace Server
{
    internal class Program
    {
        static CancellationTokenSource cancelToken = new CancellationTokenSource();

        private static void Main(string[] args)
        {
            System.Console.CancelKeyPress += Console_CancelKeyPress;

            Random rnd = new Random(BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0));
            int port = Math.Abs(rnd.Next((int)Math.Pow(2, 16) - 1));

            TestServerTCPBasic serverBasic = new TestServerTCPBasic(port);
            Task serverTask = Task.Factory.StartNew(() => serverBasic.Process(), cancelToken.Token);
            serverBasic.Request += ServerBasic_Request;
            
            while (!cancelToken.IsCancellationRequested) ;

        }

        private static void ServerBasic_Request(object sender,string requestMessage)
        {
            System.Console.WriteLine(requestMessage);
            if (requestMessage == $"End serverbasic work")
                cancelToken.Cancel(false);
        }

        private static void TestComplex(int port)
        {


            TestServerTCP serverTCP = new TestServerTCP(port);
            Task serverTCPTask = Task.Factory.StartNew(() => serverTCP.Process(), cancelToken.Token);
        }

        private static void TestServerClientNormal()
        {
            Random rnd = new Random(BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0));
            int port = Math.Abs(rnd.Next((int)Math.Pow(2, 16) - 1));

            TestServer server = new TestServer(port);
            server.ServerRequest += Server_serverRequest;
            Task serverTask = Task.Factory.StartNew(() => server.Process(), cancelToken.Token);
            TestClient client = new TestClient(port);
            client.ClientRequest += Client_ClientRequest;
            Task clientTask = Task.Factory.StartNew(() => client.Process(), cancelToken.Token);

            while (!cancelToken.IsCancellationRequested) ;

            System.Console.WriteLine("Thanks for using my App!");
        }

        private static void Client_ClientRequest(string message)
        {
            System.Console.WriteLine(message);
            if (message == $"End client work")
                cancelToken.Cancel(false);
        }

        private static void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            e.Cancel = true;
            TestClient.InterruptSignal = // uwaga! Może jeden wątek wyłączyć się wcześniej niż pierwszy, tzn client wyłączy się przed serwerem
            TestServer.InterruptSignal = true;
        }

        private static void Server_serverRequest(string message)
        {
            System.Console.WriteLine(message);
            if (message == $"End server work")
                cancelToken.Cancel(false);
        }
    }
}