﻿using Server.Console;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Client.Console
{
    public delegate void ClientRequest(string message);

    public class TestClient : IWorkflow
    {
        private readonly int portNumber;
        public TestClient(int portNumber)
        {
            this.portNumber = portNumber;
        }

        public static bool InterruptSignal { get; set; } = false;

        public event ClientRequest ClientRequest;

        public async Task<bool> Process()
        {
            while (!InterruptSignal)
            {
                await Task.Delay(500);
                var Client = new UdpClient();
                var RequestData = Encoding.ASCII.GetBytes("SomeRequestData");
                var ServerEp = new IPEndPoint(IPAddress.Any, 0);

                Client.EnableBroadcast = true;
                Client.Send(RequestData, RequestData.Length, new IPEndPoint(IPAddress.Broadcast, portNumber));
                
                var ServerResponseData = Client.Receive(ref ServerEp);
                var ServerResponse = Encoding.ASCII.GetString(ServerResponseData);
                ClientRequest?.Invoke($"Recived {0} from {1}, {ServerResponse}, {ServerEp.Address.ToString()}");

                Client.Close();
            }
            ClientRequest?.Invoke($"End client work");
            return InterruptSignal;
        }
    }
}