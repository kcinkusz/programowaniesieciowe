﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server.Console
{
    public class TestServerTCPBasic : Application, IWorkflow
    {
        private const int PORT_NO = 5000;

        public TestServerTCPBasic(int portNumber) : base(portNumber)
        {
            State = ServerState.ServerReady;
        }

        private CancellationTokenSource token = new CancellationTokenSource();
        public ServerState State { get; private set; } = ServerState.ServerReady;
        private ManualResetEvent allDone = new ManualResetEvent(false);

        public Task<bool> Process2()
        {
            String strHostName = Dns.GetHostName();
            IPHostEntry iphostentry = Dns.GetHostByName(strHostName);
            if (iphostentry.AddressList.Any())
            {
                IPEndPoint localEndPoint = new IPEndPoint(iphostentry.AddressList.Last(), PORT_NO);
                Socket listener = new Socket(iphostentry.AddressList.Last().AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                // Bind the socket to the local endpoint and listen for incoming connections.
                try
                {
                    listener.Bind(localEndPoint);
                    listener.Listen(100);

                    while (true)
                    {
                        // Set the event to nonsignaled state.
                        allDone.Reset();

                        // Start an asynchronous socket to listen for connections.
                        OnRequest(this, "Waiting for a connection...");
                        listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);
                        // Wait until a connection is made before continuing.
                        allDone.WaitOne();
                    }
                }
                catch (Exception e)
                {
                    OnRequest(this, e.ToString());
                }

                OnRequest(this, "\nPress ENTER to continue...");
            }
            return null;
        }

        public void AcceptCallback(IAsyncResult ar)
        {
            // Signal the main thread to continue.
            allDone.Set();

            // Get the socket that handles the client request.
            Socket listener = (Socket)ar.AsyncState;
            Socket handler = listener.EndAccept(ar);

            // Create the state object.
            StateObject state = new StateObject();
            state.workSocket = handler;
            handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                new AsyncCallback(ReadCallback), state);
        }

        public void ReadCallback(IAsyncResult ar)
        {
            String content = String.Empty;

            // Retrieve the state object and the handler socket
            // from the asynchronous state object.
            StateObject state = (StateObject)ar.AsyncState;
            Socket handler = state.workSocket;

            // Read data from the client socket.
            int bytesRead = handler.EndReceive(ar);

            if (bytesRead > 0)
            {
                // There  might be more data, so store the data received so far.
                state.sb.Append(Encoding.ASCII.GetString(
                    state.buffer, 0, bytesRead));

                // Check for end-of-file tag. If it is not there, read
                // more data.
                content = state.sb.ToString();
                if (content.IndexOf("<EOF>") > -1)
                {
                    // All the data has been read from the
                    // client. Display it on the console.
                    OnRequest(this, $"Read {0} bytes from socket. \n Data : {1}, {content.Length}, {content}");
                    // Echo the data back to the client.
                    Send(handler, content);
                }
                else
                {
                    // Not all data received. Get more.
                    handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReadCallback), state);
                }
            }
        }

        private void Send(Socket handler, String data)
        {
            // Convert the string data to byte data using ASCII encoding.
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote device.
            handler.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), handler);
        }

        private void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket handler = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.
                int bytesSent = handler.EndSend(ar);
                OnRequest(this, $"Sent {0} bytes to client., {bytesSent}");

                handler.Shutdown(SocketShutdown.Both);
                handler.Close();
            }
            catch (Exception e)
            {
                OnRequest(this, e.ToString());
            }
        }

        public Task<bool> Process()
        {
            #region Get ipaddres of route computer interfaces

            // Get host name
            String strHostName = Dns.GetHostName();

            // Find host by name
            IPHostEntry iphostentry = Dns.GetHostByName(strHostName);

            // Enumerate IP addresses
            foreach (IPAddress ipaddress in iphostentry.AddressList)
            {
                //   ....
            }

            #endregion Get ipaddres of route computer interfaces

            var asd = Helper.AllInterfaces;
            var asdasdas = Helper.GetIpOfAllNetworkInterfaces().ToList();

            Tuple<string, IPAddress> netInterface = Helper.GetIpOfAllNetworkInterfaces().Last();

            //IPEndPoint endPointFalse = new IPEndPoint(netInterface.Item2, portNumber);

            IPEndPoint endPoint = new IPEndPoint(iphostentry.AddressList.Last(), PORT_NO);
            //IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, portNumber);
            TcpListener listener = new TcpListener(endPoint);
            listener.Start();

            while (!InterruptSignal)
            {
                if (listener.Pending())
                {
                    TcpClient client = listener.AcceptTcpClient();
                    if (client != null && State == ServerState.ServerReady)
                    {
                        //byte[] buffer = ASCIIEncoding.ASCII.GetBytes(State.ToString());
                        //IAsyncResult asyncResult = client.Client.BeginSend(buffer, 0, buffer.Length, SocketFlags.None, new AsyncCallback(this.CompleteMessageSending), client.Client);
                        //while (asyncResult.AsyncWaitHandle.WaitOne()) ;

                        byte[] data = new byte[client.ReceiveBufferSize];
                        client.Client.Receive(data);
                        OnRequest(this, Helper.ReceivedData);
                        StringBuilder sb = new StringBuilder();
                        for (int i = 0; i < client.ReceiveBufferSize; i++)
                            sb.Append(Convert.ToChar(data[i]));
                        OnRequest(this, $"{sb}{Helper.NewLine}");
                        client.Close();
                        token.Cancel(false);
                        State = ServerState.ServerReady;
                    }
                    else if (client != null && State == ServerState.ServerBusy)
                    {
                    }
                }
                else
                {
                    OnRequest(this, Helper.Sleep);
                    Thread.Sleep(1000);
                }
            }
            OnRequest(this, Helper.EndServerWork);
            OnRequest(this, Helper.InterruptServerSignal);
            return null;
        }

        public Task<bool> GetDataFromClient(TcpClient client)
        {
            if (client != null)
            {
                byte[] buffer = ASCIIEncoding.ASCII.GetBytes(State.ToString());
                IAsyncResult asyncResult = client.Client.BeginSend(buffer, 0, buffer.Length, SocketFlags.None, new AsyncCallback(this.CompleteMessageSending), client.Client);
                asyncResult.AsyncWaitHandle.WaitOne();

                byte[] data = new byte[client.ReceiveBufferSize];
                client.Client.Receive(data);
                OnRequest(this, Helper.ReceivedData);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < client.ReceiveBufferSize; i++)
                    sb.Append(Convert.ToChar(data[i]));
                OnRequest(this, $"{sb}{Helper.NewLine}");
                client.Close();
                token.Cancel(false);
                State = ServerState.ServerReady;
            }
            return null;
        }

        private void CompleteMessageSending(IAsyncResult asyncResult)
        {
            Socket client = asyncResult.AsyncState as Socket;
            client.EndSend(asyncResult);
        }

        public void SendMessage(Socket socket, string message)
        {
            byte[] buffer = Encoding.ASCII.GetBytes(message);
            socket.BeginSend(buffer, 0, buffer.Length, SocketFlags.None, new AsyncCallback(this.CompleteMessageSending), socket);
        }
    }
}