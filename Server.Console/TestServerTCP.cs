﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Server.Console
{
    public class TcpListenerExtended : TcpListener
    {
        public TcpListenerExtended(IPEndPoint localEP) : base(localEP)
        {
        }

        public TcpListenerExtended(int port) : base(port)
        {
        }

        public TcpListenerExtended(IPAddress localaddr, int port) : base(localaddr, port)
        {
        }

        public Task listenerTask { get; set; }
    }
    public delegate void ServerTCPRequest(string message);
    
    public class TestServerTCP : IWorkflow
    {
        public ServerState state { get; private set; }
        private readonly int portNumber;
        public TestServerTCP(int portNumber)
        {
            this.portNumber = portNumber;
            state = ServerState.ServerReady;
        }

        public static bool InterruptSignal { get; set; } = false;

        public event ServerRequest ServerRequest;

        public async Task<bool> Process()
        {
            Random rnd = new Random(BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0));

            List<Tuple<string, IPAddress>> list = Helper.GetIpOfAllNetworkInterfaces().ToList();
            List<TcpListenerExtended> listeners = new List<TcpListenerExtended>();
            List<IPEndPoint> ipEndPoints = new List<IPEndPoint>();
            list.ForEach(netInterfaces =>
            {
                int port = Math.Abs(rnd.Next((int)Math.Pow(2, 16) - 1));
                IPEndPoint endPoint = new IPEndPoint(netInterfaces.Item2, port);
                ipEndPoints.Add(endPoint);
                TcpListenerExtended listener = new TcpListenerExtended(endPoint);
                listener.Start();
                listener.listenerTask = Task.Factory.StartNew(async () =>
                {
                    while (true)
                    {
                        TcpClient client = await TcpClientFromEndPoint(endPoint);

                        //var childSocketThread = new Thread(() =>
                        //{
                        //    byte[] data = new byte[100];
                        //    int size = client.Receive(data);
                        //    Console.WriteLine("Recieved data: ");
                        //    for (int i = 0; i < size; i++)
                        //        Console.Write(Convert.ToChar(data[i]));

                        //    Console.WriteLine();

                        //    client.Close();
                        //});
                        //childSocketThread.Start();
                    }
                });
                listeners.Add(listener);
            });


            int givenPortNumber = 7;
            var Server = new UdpClient(givenPortNumber);
            string clientRequestCommunicate = "DISCOVER";
            byte[] clientRequestCommunicateBytes = Encoding.ASCII.GetBytes(clientRequestCommunicate);
            string serverRequestCommunicate = "OFFER ADDRESS PORT";
            byte[] serverRequestCommunicateBytes = Encoding.ASCII.GetBytes(serverRequestCommunicate);
            byte[] ResponseData = Encoding.ASCII.GetBytes("SomeResponseData");

            while (!InterruptSignal)
            {
                await Task.Delay(3000);
                ipEndPoints.ForEach(endPoint =>
                {
                    var ClientRequestData = Server.Receive(ref endPoint);
                    var ClientRequest = Encoding.ASCII.GetString(clientRequestCommunicateBytes);
                    // ServerRequest?.Invoke($"Recived {0} from {1}, sending response {ClientRequest}, {ClientEp.Address.ToString()}");
                    Server.Send(serverRequestCommunicateBytes, serverRequestCommunicateBytes.Length, endPoint);
                });


            }
            ServerRequest?.Invoke($"End server work");
            return InterruptSignal;
        }
        
        public async Task<TcpClient> TcpClientFromEndPoint(IPEndPoint endPoint)
        {
            TcpListenerExtended listener = new TcpListenerExtended(endPoint);
            listener.Start();
            return await listener.AcceptTcpClientAsync();
        }
    }
}