﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace Server.Console
{
    public class TestClientTCPBasic : Application, IWorkflow
    {
        public TestClientTCPBasic(int portNumber) : base(portNumber)
        {
        }

        public Task<bool> Process()
        {
            String strHostName = Dns.GetHostName();
            IPHostEntry iphostentry = Dns.GetHostByName(strHostName);
            IPEndPoint endPoint = new IPEndPoint(iphostentry.AddressList.First(), portNumber);
            TcpClient client = new TcpClient(Helper.globalEndPoint);
            while (!InterruptSignal)
            {
                if (client.Connected)
                {
                    client.Client.SendBufferSize = Helper.simpleCommunicateBytes.Length;
                    client.Client.Send(Helper.simpleCommunicateBytes);
                    OnRequest(this, "Wysłałem, a teraz idę spać");
                    Thread.Sleep(7000);
                }
                Thread.Sleep(500);
            }
            client.Close();
            return null;
        }
    }
}