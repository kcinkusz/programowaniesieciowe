﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Server.Console
{
    public delegate void ServerRequest(string message);

    public class TestServer : IWorkflow
    {
        private readonly int portNumber;
        public TestServer(int portNumber)
        {
            this.portNumber = portNumber;
        }

        public static bool InterruptSignal { get; set; } = false;

        public event ServerRequest ServerRequest;

        public async Task<bool> Process()
        {
            var Server = new UdpClient(portNumber);
            var ResponseData = Encoding.ASCII.GetBytes("SomeResponseData");
            
            while (!InterruptSignal)
            {
                await Task.Delay(3000);
                var ClientEp = new IPEndPoint(IPAddress.Any, 0);
                var ClientRequestData = Server.Receive(ref ClientEp);
                var ClientRequest = Encoding.ASCII.GetString(ClientRequestData);
                ServerRequest?.Invoke($"Recived {0} from {1}, sending response {ClientRequest}, {ClientEp.Address.ToString()}");
                Server.Send(ResponseData, ResponseData.Length, ClientEp);
            }
            ServerRequest?.Invoke($"End server work");
            return InterruptSignal;
        }
    }
}