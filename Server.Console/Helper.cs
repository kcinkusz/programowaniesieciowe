﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;

namespace Server.Console
{

    public static class Helper
    {
        public const string ReceivedData = "Received data:";
        public const string NewLine = "\n";
        public const string Sleep = "Sleep under next pending connection...";
        public const string EndServerWork = "End serverbasic work";
        public const string InterruptClientSignal = "CF8F4016-38AF-49B0-8DBA-27278958404E";
        public const string InterruptServerSignal = "45F9FE72-4380-471C-905B-E8598F12CE44";
        public static IPEndPoint globalEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 5000);
        public static string simpleCommunicate = "OFFER ADDRESS PORT";
        public static byte[] simpleCommunicateBytes = Encoding.ASCII.GetBytes(simpleCommunicate);

        public static NetworkInterface[] AllInterfaces { get; } = NetworkInterface.GetAllNetworkInterfaces();

        public static IEnumerable<Tuple<string, IPAddress>> GetIpOfAllNetworkInterfaces()
        {
            foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 || ni.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                {
                    foreach (UnicastIPAddressInformation ip in ni.GetIPProperties().UnicastAddresses)
                    {
                        if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                        {
                            yield return new Tuple<string, IPAddress>(ni.Name, ip.Address);
                        }
                    }
                }
            }
        }
    }
}