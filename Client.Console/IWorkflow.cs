﻿using System.Threading.Tasks;

namespace Client.Console
{
    /// <summary>
    /// Interfejs opisujący klasy, którę mają stałą prace do wykonaniam
    /// </summary>
    public interface IWorkflow
    {
        Task<bool> Process();
    }
}