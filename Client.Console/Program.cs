﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Client.Console
{
    internal class Program
    {
        static CancellationTokenSource cancelToken = new CancellationTokenSource();

        private static void Main(string[] args)
        {
            System.Console.CancelKeyPress += Console_CancelKeyPress;

            Random rnd = new Random(BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0));
            int port = Math.Abs(rnd.Next((int)Math.Pow(2, 16) - 1));

            TestClientTCPBasic clientBasic = new TestClientTCPBasic(port);
            Task clientTask = Task.Factory.StartNew(() => clientBasic.Process(), cancelToken.Token);
            clientBasic.Request += ClientBasic_Request;


            while (!cancelToken.IsCancellationRequested) ;
        }

        private static void ClientBasic_Request(object sender,string requestMessage)
        {
            System.Console.WriteLine(requestMessage);
            if (requestMessage == $"End serverbasic work")
                cancelToken.Cancel(false);
        }

        private static void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            e.Cancel = true;
            TestClientTCPBasic.InterruptSignal = true;
        }

    }
}