﻿namespace Client.Console
{
    public delegate void Request(object sender, string requestMessage);

    public abstract class Application
    {
        protected readonly int portNumber;

        public Application(int portNumber)
        {
            this.portNumber = portNumber;
        }

        public event Request Request;

        protected virtual void OnRequest(object sender, string message) => Request?.Invoke(sender, message);

        public static bool InterruptSignal { get; set; } = false;
    }
}