﻿using Server.Console;
using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TCP.Core;

namespace Client.Console
{
    public class TestClientTCPBasic : Application, IWorkflow
    {
        public TestClientTCPBasic(int portNumber) : base(portNumber)
        {
        }

        private const int PORT_NO = 5000;
        private const string SERVER_IP = "127.0.0.1";
        private ManualResetEvent connectDone = new ManualResetEvent(false);
        private ManualResetEvent sendDone = new ManualResetEvent(false);
        private ManualResetEvent receiveDone = new ManualResetEvent(false);
        private string response = string.Empty;
        public Task<bool> Process2()
        {
            String strHostName = Dns.GetHostName();
            IPHostEntry iphostentry = Dns.GetHostByName(strHostName);
            if (iphostentry.AddressList.Any())
            {
                IPEndPoint endPoint = new IPEndPoint(iphostentry.AddressList.Last(), PORT_NO);
                Socket client = new Socket(iphostentry.AddressList.Last().AddressFamily, SocketType.Stream, ProtocolType.Tcp);
               
                client.BeginConnect(endPoint, new AsyncCallback(ConnectCallback), client);
                connectDone.WaitOne();

                Send(client, "Test data");
                sendDone.WaitOne();

                Receive(client);
                receiveDone.WaitOne();

                OnRequest(this, $"Otrzymano odpowiedź: {response}");

                client.Shutdown(SocketShutdown.Both);
                client.Close();
            }
            return null;
        }
        private void Receive(Socket client)
        {
            try
            {
                // Create the state object.  
                StateObject state = new StateObject();
                state.workSocket = client;

                // Begin receiving the data from the remote device.  
                client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReceiveCallback), state);
            }
            catch (Exception e)
            {
                OnRequest(this, e.ToString());
            }
        }
        private void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the state object and the client socket   
                // from the asynchronous state object.  
                StateObject state = (StateObject)ar.AsyncState;
                Socket client = state.workSocket;

                // Read data from the remote device.  
                int bytesRead = client.EndReceive(ar);

                if (bytesRead > 0)
                {
                    // There might be more data, so store the data received so far.  
                    state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));

                    // Get the rest of the data.  
                    client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                        new AsyncCallback(ReceiveCallback), state);
                }
                else
                {
                    // All the data has arrived; put it in response.  
                    if (state.sb.Length > 1)
                    {
                        response = state.sb.ToString();
                    }
                    // Signal that all bytes have been received.  
                    receiveDone.Set();
                }
            }
            catch (Exception e)
            {
                OnRequest(this, e.ToString());
            }
        }
        private void Send(Socket client, String data)
        {
            // Convert the string data to byte data using ASCII encoding.  
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote device.  
            client.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), client);
        }
        private  void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.  
                Socket client = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.  
                int bytesSent = client.EndSend(ar);
                OnRequest(this, $"Sent {bytesSent} bytes to server.");

                // Signal that all bytes have been sent.  
                sendDone.Set();
            }
            catch (Exception e)
            {
               OnRequest(this, e.ToString());
            }
        }
        private void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                Socket client = (Socket)ar.AsyncState;
                client.EndConnect(ar);

                OnRequest(this, $"Socket connected to {0}, {client.RemoteEndPoint.ToString()}");

                connectDone.Set();
            }
            catch (Exception e)
            {
                OnRequest(this, e.ToString());
            }
        }
        public Task<bool> Process()
        {
            string textToSend = DateTime.Now.ToString();
            String strHostName = Dns.GetHostName();
            IPHostEntry iphostentry = Dns.GetHostByName(strHostName);
            IPEndPoint endPoint = new IPEndPoint(iphostentry.AddressList.Last(), PORT_NO);
           
            while (!InterruptSignal)
            {
                TcpClient client = new TcpClient(iphostentry.AddressList.Last().ToString(), PORT_NO);
                
                if (client.Connected)
                {
                    NetworkStream nwStream1 = client.GetStream();
                    byte[] bytesToSend1 = ASCIIEncoding.ASCII.GetBytes(Server.Console.ServerState.ServerBusy.ToString());
                    //---send the text---
                    OnRequest(this, "Sending : " + textToSend);
                    nwStream1.Write(bytesToSend1, 0, bytesToSend1.Length);

                    byte[] data = new byte[client.ReceiveBufferSize];
                    client.Client.Receive(data);
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < client.ReceiveBufferSize; i++)
                    {
                        var s = Convert.ToChar(data[i]).ToString();
                        if (s != "\0")
                            sb.Append(s);
                    }
                    //string state = sb.ToString();

                    //if (state == ServerState.ServerBusy.ToString())
                    //    client.Close();
                    //else if (state == ServerState.ServerReady.ToString())
                    //{
                    //    //client.Client.Receive
                    //    NetworkStream nwStream = client.GetStream();
                    //    byte[] bytesToSend = ASCIIEncoding.ASCII.GetBytes(textToSend);

                    //    //---send the text---
                    //    OnRequest(this, "Sending : " + textToSend);
                    //    nwStream.Write(bytesToSend, 0, bytesToSend.Length);

                    //    //client.Client.SendBufferSize = Helper.simpleCommunicateBytes.Length;
                    //    //client.Client.Send(Helper.simpleCommunicateBytes);
                    //    OnRequest(this, "Wysłałem, a teraz idę spać");
                    //}
                    //else
                    //{
                    //    throw new Exception("Wystąpił nieoczekiwany wyjątek");
                    //}
                }
                client.Close();
                Thread.Sleep(500);
            }
            return null;
        }
    }
}