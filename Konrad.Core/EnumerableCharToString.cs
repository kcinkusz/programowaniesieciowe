﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Konrad.Core
{
    public static class EnumerableCharToString
    {
        //TODO: Konrad - add test method
        private static readonly IEnumerable<char> seq = Enumerable.Repeat('a', 300);

        public static string Concat(IEnumerable<char> charSequence)
        {
            return String.Concat(charSequence);
        }

        public static string StringBuilderChars(IEnumerable<char> charSequence)
        {
            var sb = new StringBuilder();
            foreach (var c in charSequence)
            {
                sb.Append(c);
            }
            return sb.ToString();
        }

        public static string StringBuilderStrings(IEnumerable<char> charSequence)
        {
            var sb = new StringBuilder();
            foreach (var c in charSequence)
            {
                sb.Append(c.ToString());
            }
            return sb.ToString();
        }

        public static string ToArrayString(IEnumerable<char> charSequence)
        {
            return new String(charSequence.ToArray());
        }
    }
}