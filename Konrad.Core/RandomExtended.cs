﻿using System;

namespace Konrad.Core
{
    public enum SeedGeneratorMethod
    {
        None,
        Classic,
        GuidHashCode,
        GuidBitConverter,
    }

    public class RandomExtended : Random
    {
        //public int Seed { get; set; } = 1;
        private readonly int? seedInternal;

        public static Random Build(SeedGeneratorMethod seedGeneratorMethod)
        {
            Random random;
            switch (seedGeneratorMethod)
            {
                case SeedGeneratorMethod.None:
                    random = new Random();
                    break;

                case SeedGeneratorMethod.Classic:
                    random = new Random(1);
                    break;

                case SeedGeneratorMethod.GuidHashCode:
                    random = new Random(Guid.NewGuid().GetHashCode());
                    break;

                case SeedGeneratorMethod.GuidBitConverter:
                    random = new Random(BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0));
                    break;

                default:
                    throw new NotImplementedException("Musisz wybrać metodę generowania ziarna (seeda)");
            }

            RandomExtended ran = new RandomExtended(SeedGeneratorMethod.Classic);
                    return ran;
        }

        private RandomExtended(SeedGeneratorMethod seedGeneratorMethod) : base()
        {
            switch (seedGeneratorMethod)
            {
                case SeedGeneratorMethod.None:
                    seedInternal = null;
                    break;

                case SeedGeneratorMethod.Classic:
                    seedInternal = 1;
                    break;

                case SeedGeneratorMethod.GuidHashCode:
                    seedInternal = Guid.NewGuid().GetHashCode();
                    break;

                case SeedGeneratorMethod.GuidBitConverter:
                    seedInternal = BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0);
                    break;

                default:
                    throw new NotImplementedException("Musisz wybrać metodę generowania ziarna (seeda)");
            }
        }
    }
}