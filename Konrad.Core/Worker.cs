﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Konrad.Core
{
    internal static class Worker
    {
        private static readonly BlockingCollection<Action> m_actions =
            new BlockingCollection<Action>();

        static Worker()
        {
            Task.Run(() =>
            {
                foreach (var action in m_actions.GetConsumingEnumerable())
                {
                    try { action(); } catch (Exception e) { Debug.WriteLine(e); }
                }
            });
        }

        public static void Clear()
        {
            Action dumped;
            while (m_actions.TryTake(out dumped)) ;
        }

        public static void Enqueue(Action action)
        {
            m_actions.Add(action);
        }
    }
}