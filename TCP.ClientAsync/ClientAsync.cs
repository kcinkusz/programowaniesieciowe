﻿using Konrad.Core;
using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using TCP.Core;

namespace TCP.ClientAsync
{
    public class ClientAsync : Application, IClient
    {
        private static readonly object padlock = new object();
        private static ClientAsync instance = null;
        private ManualResetEvent connectDone = new ManualResetEvent(false);
        private ClientAsync()
        {
        }

        public static ClientAsync Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new ClientAsync();
                    }
                    return instance;
                }
            }
        }

        public ClientState ClientState { get; set; } = ClientState.ClientNotRunned;

        Guid IClient.Guid => base.Guid;
        public Task<bool> Process()
        {
            try
            {
                IPAddress address = Helper.GetIPAddressesOfRouteComputerInterfaces().Last();

                IPEndPoint endPoint = new IPEndPoint(address, Helper.DEFAULT_PORT_NO);

                Socket clientSocket = new Socket(address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                clientSocket.BeginConnect(endPoint, new AsyncCallback(ConnectCallback), clientSocket);
            }
            catch (Exception ex)
            {
                OnRequest(this, ex.ToString(), new TextFormatting());
            }
            return null; // chyba że zrobimy asynca
        }

        private void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                Socket client = (Socket)ar.AsyncState;
                client.EndConnect(ar);

                OnRequest(this, $"Socket connected to {0}, {client.RemoteEndPoint.ToString()}", new TextFormatting());

                connectDone.Set();
            }
            catch (Exception e)
            {
                OnRequest(this, e.ToString(), new TextFormatting());
            }
        }
    }
}