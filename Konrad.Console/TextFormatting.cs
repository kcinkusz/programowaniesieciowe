﻿using System;

namespace Konrad.Console
{
    public class TextFormatting : Konrad.Core.TextFormatting
    {
        private ConsoleColor consoleColor;

        public ConsoleColor ConsoleColor
        {
            get
            {
                ConsoleColor.TryParse(ColorName, out consoleColor);
                return consoleColor;
            }
            set { ColorName = value.ToString(); }
        }
    }
}